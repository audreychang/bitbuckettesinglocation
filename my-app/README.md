This project is bootstrapped with [Create Mace App](https://bitbucket.org/maceprogrammecontrols/create-mace-app).

Below you will find some information on how to perform basic tasks.  
You can find the most recent version of this guide [here](template/README.md?at=master).

## Table of Contents
- [Sending feedback](#markdown-header-sending-feedback)
- [Folder structure](#markdown-header-folder-structure)
- [Installing JavaScript packages](#markdown-header-installing-javascript-packages)
- [Available scripts](#markdown-header-available-scripts)
  - [mace-app build](#markdown-header-mace-app-build)
  - [mace-app start](#markdown-header-mace-app-start)
  - [mace-app <elm-platform-command>](#markdown-header-mace-app-elm-platform-comand)
    - [package](#markdown-header-package)
    - [repl](#markdown-header-repl)
    - [make](#markdown-header-make)
    - [reactor](#markdown-header-reactor)
- [Adding a Stylesheet](#markdown-header-adding-a-stylesheet)
- [Adding Images and Fonts](#markdown-header-adding-images-and-fonts)
- [Using the `src/html` Folder](#markdown-header-using-the-public-folder)
  - [Changing the HTML](#markdown-header-changing-the-html)
  - [Adding Assets Outside of the Module System](#markdown-header-adding-assets-outside-of-the-module-system)
  - [When to Use the `public` Folder](#markdown-header-when-to-use-the-public-folder)
- [Setting up API Proxy](#markdown-header-setting-up-api-proxy)
- [Running tests](#markdown-header-running-tests)
  - [Dependencies in Tests](#markdown-header-dependencies-in-tests)
- [Deployment](#markdown-header-deployment)
  - [Before Your First Deployment](#markdown-header-before-your-first-deployment)
  - [Static Server](#markdown-header-static-server)
- [IDE setup for StandarJS](#markdown-header-ide-setup-for-standardjs)
- [IDE setup for Hot Module Replacement](#markdown-header-ide-setup-for-hot-module-replacement)

## Sending feedback

You are very welcome with any [feedback](https://bitbucket.org/maceprogrammecontrols/create-mace-app/issues?status=new&status=open)

## Folder structure
```
my-app/
  .gitignore
  README.md
  elm-package.json
  src/
    html/
    js/
    css/
    favicon.ico
  functions/
    index.js
    package.json
  tests/
    elm-package.json
    Tests.elm
```
For the project to build, these files must exist with exact filenames:

- `src/html/index.html` is the page template;
- `src/favicon.ico` is the icon you see in the browser tab;
- `src/js/index.js` is the JavaScript entry point.

You may create subdirectories inside src.

## Installing JavaScript packages

To use JavaScript packages from npm, you'll need to add a `package.json`, install the dependencies, and you're ready to go.

```sh
npm init -y # Add package.json
npm install --save-dev pouchdb-browser # Install library from npm
```

```js
// Use in your JS code
import PouchDB from 'pouchdb-browser';
const db = new PouchDB('mydb');
```

## Available scripts
In the project directory you can run:
### `mace-app start`
Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.

### `mace-app build`
Builds the app for production to the `build` folder.  

The build is minified, and the filenames include the hashes.  

**Note:** *Before running this command please refer to [Deployment](#markdown-header-deployment) section.*

### `mace-app <elm-platform-comand>`
Create Mace App does not rely on the global installation of Elm Platform, but you still can use it's local Elm Platform to access default command line tools:

#### `package`
Alias for [elm-package](http://guide.elm-lang.org/get_started.html#elm-package)

Use it for installing Elm packages from [package.elm-lang.org](http://package.elm-lang.org/)

#### `repl`
Alias for [elm-repl](http://guide.elm-lang.org/get_started.html#elm-repl)

#### `make`
Alias for  [elm-make](http://guide.elm-lang.org/get_started.html#elm-make)

#### `reactor`
Alias for  [elm-reactor](http://guide.elm-lang.org/get_started.html#elm-reactor)

## Adding a Stylesheet

This project setup uses [Webpack](https://webpack.js.org/) for handling all assets. Webpack offers a custom way of “extending” the concept of `import` beyond JavaScript. To express that a JavaScript file depends on a CSS file, you need to **import the CSS from the JavaScript file**:

### `main.css`

```css
body {
  padding: 20px;
}
```

### `index.js`

```js
import './main.css'; // Tell Webpack to pick-up the styles from base.css
```

In development, expressing dependencies this way allows your styles to be reloaded on the fly as you edit them. In production, all CSS files will be concatenated into a single minified `.css` file in the build output.

You can put all your CSS right into `src/main.css`. It would still be imported from `src/index.js`, but you could always remove that import if you later migrate to a different build tool.

## Adding Images and Fonts

With Webpack, using static assets like images and fonts works similarly to CSS.

By requiring an image in JavaScript code, you tell Webpack to add a file to the build of your application. The variable will contain a unique path to the said file.



Here is an example with vanilla JavaScript:

In the standard dashboard index.html file there is a logo in the upper left corner.
```html
<div class="topbar-left">
    <img id="logo-id"  alt="Dashboard Logo" />
</div>
```
Notice that there is no src attribute specified at this point. It will need to be injected dynamically in the JS.

So, in the corresponding JS script file

```js
/* Image resources */
import logoPath from '../assets/clarion-hg-logo.png'
 // Tell Webpack this JS file uses this image

```
Then in an appropriate content ready script ...
```js
// Assuming jQuery is available in the $ varible!
$('#logo-id').attr('src', logoPath)
```


## Using the `src/html` Folder

### Changing the HTML

The `src/html` folder contains the HTML file so you can tweak it.
The `<script>` tag with the compiled code will be added to it automatically during the build process.

### Adding Assets Outside of the Module System

You can also add other assets to the `src/hmtl` folder.

Note that we normally encourage you to `import` assets in JavaScript files instead.
For example, see the sections on [adding a stylesheet](#markdown-header-adding-a-stylesheet) and [adding images and fonts](#markdown-header-adding-images-fonts-and-files).
This mechanism provides a number of benefits:

* Scripts and stylesheets get minified and bundled together to avoid extra network requests.
* Missing files cause compilation errors instead of 404 errors for your users.
* Result filenames include content hashes so you don’t need to worry about browsers caching their old versions.

However there is an **escape hatch** that you can use to add an asset outside of the module system.

If you put a file into the `src/html` folder, it will **not** be processed by Webpack. Instead it will be copied into the build folder untouched.   To reference assets in the `public` folder, you need to use a special variable called `PUBLIC_URL`.

Inside `index.html`, you can use it like this:

```html
<link rel="shortcut icon" href="%PUBLIC_URL%/favicon.ico">
```

Only files inside the `src/html` folder will be accessible by `%PUBLIC_URL%` prefix. If you need to use a file from `src` or `node_modules`, you’ll have to copy it there to explicitly specify your intention to make this file a part of the build.

When you run `npm run build`, Create Mace App will substitute `%PUBLIC_URL%` with a correct absolute path so your project works even if you use client-side routing or host it at a non-root URL.

In Elm code, you can use `%PUBLIC_URL%` for similar purposes:

```elm
// Note: this is an escape hatch and should be used sparingly!
// Normally we recommend using `import`  and `Html.programWithFlags` for getting 
// asset URLs as described in “Adding Images and Fonts” above this section.
img [ src "%PUBLIC_PATH%/logo.svg" ] []
```

In JavaScript code, you can use `process.env.PUBLIC_URL` for similar purposes:

```js
const logo = `<img src=${process.env.PUBLIC_URL + '/img/logo.svg'} />`;
```

Keep in mind the downsides of this approach:

* None of the files in `public` folder get post-processed or minified.
* Missing files will not be called at compilation time, and will cause 404 errors for your users.
* Result filenames won’t include content hashes so you’ll need to add query arguments or rename them every time they change.

### When to Use the `src/html` Folder

Normally we recommend importing [stylesheets](#adding-a-stylesheet), [images, and fonts](#adding-images-fonts-and-files) from JavaScript.
The `src/html` folder is useful as a workaround for a number of less common cases:

* You need a file with a specific name in the build output, such as [`manifest.webmanifest`](https://developer.mozilla.org/en-US/docs/Web/Manifest).
* You have thousands of images and need to dynamically reference their paths.
* You want to include a small script like [`pace.js`](http://github.hubspot.com/pace/docs/welcome/) outside of the bundled code.
* Some library may be incompatible with Webpack and you have no other option but to include it as a `<script>` tag.

Note that if you add a `<script>` that declares global variables, you also need to read the next section on using them.


## Setting up API Proxy
To forward the API ( REST ) calls to backend server, add a proxy to the `elm-package.json` in the top level json object.

```json
{
    ...
    "proxy" : "http://localhost:1313",
    ...
}
```

Make sure the XHR requests set the `Content-type: application/json` and `Accept: application/json`.
The development server has heuristics, to handle it's own flow, which may interfere with proxying of 
other html and javascript content types.

```sh
 curl -X GET -H "Content-type: application/json" -H "Accept: application/json"  http://localhost:3000/api/list
```

## Running Tests

Create Mace App uses [elm-test](https://github.com/rtfeldman/node-test-runner) as its test runner.

### Dependencies in Tests

To use packages in tests, you also need to install them in `tests` directory.

```bash
mace-app test --add-dependencies elm-package.json 
```

## Deployment


`mace-app build` creates a `build` directory with a production build of your app. Set up your favourite HTTP server so that a visitor to your site is served `index.html`, and requests to static paths like `/static/js/main.<hash>.js` are served with the contents of the `/static/js/main.<hash>.js` file.

### Before Your First Deployment

Create Mace App comes with a predefined configuration so you and run:

```sh 
mace-app start
```

But that configuration should be changed to reflect your project's information.

Please follow the steps bellow so your application is configured correctly before your **first deployment**.

1. create a Firebase project.
    1. enable *Google for Sign-in Method* under Firebase Authentication,
    2. click the **cog icon** from the left menu navigation (next to the **Overview** button), and select **Project settings**,
    3. from **Your apps** section select **Add Firebase to your web app**,
    4. take note of the configuration and update **src/js/dash-config.js -> firebaseConf object** with your Firebase configuration.
2. enable APIs and configure *Service Account*(with domain delegation) for your Firebase project.
    1. head to [Google Developers Console](https://console.developers.google.com) and select the project related to the one created on step 1,
    2. on the left side menu select **Library** and enable *at least* the following APIs: **Drive API, Sheets API and Admin SDK** under **G Suite APIs** section,
    3. with APIs enabled, click on the sandwich bar and select **IAM & Admin -> Service Accounts**,
    4. then click the top blue link **Create Service Account**,
    5. give a name for this service account,
    6. under **Role** dropdown select **Project -> Owner**,
    7. make sure you **select** both **Furnish a new private key** (keep **Key type** as JSON) and **Enable G Suite Domain-wide Delegation**,
    8. finally click the *Create* button,
    9. save the file where you can access it later,
    10. head to **functions/index.js -> googleJWTClient constant** and update the values with the ones from the downloaded JSON.
3. configure your G Suite to authorise the Service Account you created to access your G Suite data (based on the APIs you enabled).
    1. head to [Manage API Client access](https://admin.google.com/AdminHome?chromeless=1#OGX:ManageOauthClients) in the G Suite Admin Console,
    2. from the JSON file saved on step 2.9, copy the value related to the **client_id** and paste into **Client name** input field,
    3. on **One or More API Scopes** input field, paste the line below (in that order!!!). They should match the APIs you enabled on step 2.2: 
		https://www.googleapis.com/auth/drive, https://www.googleapis.com/auth/spreadsheets, https://www.googleapis.com/auth/admin.directory.group.member.readonly, https://www.googleapis.com/auth/admin.directory.group.readonly
        
    4. finally click **Authorize**
4. initialise Firebase locally and deploy Firebase functions.
    1. on the root of your app folder run ```firebase login```. Make sure you login with the right account for your G Suite,
    2. now execute 
        ``` 
        firebase init
        ```
    3. select **Database, Functions and Hosting** by pressing <space> on each option if not already selected. Press <enter>,
    4. select the project you created on Firebase website,
    5. for **What file should be used for Database Rules** use the default answer,
    6. for **File functions/package.json already exists. Overwrite?** answer with **N** (for No),
    7. for **File functions/index.js already exists. Overwrite?** answer with **N**,
    8. for **Do you want to install dependencies with npm now?** answer with **Y**,
    9. for **What do you want to use as your public directory?** answer with **public**,
    10. for **Configure as a single-page app (rewrite all urls to /index.html)?** answer with **N**,
    11. now execute 
        ```
        firebase deploy --only functions
        ```
    12. copy the url for the function **getAccessToken** and paste into **src/js/dash-config.js -> dashConf -> tokenGeneratorUrl**.
5. on the root folder of your app, you can now run 
    ``` 
    mace-app build
    ```
    Bundled files will be created under **public/** folder,
6. run 
    ``` 
    firebase deploy --only hosting
    ``` 
    from the root folder so the **public/** folder can be deployed to Firebase Hosting,
7. access your app url to check whether or not its working. 

**OBS:** Data source is public so your deployed version should still work.

### Static Server

For environments using [Node](https://nodejs.org/), the easiest way to handle this would be to install [serve](https://github.com/zeit/serve) and let it handle the rest:

```
npm install -g serve
serve -s build
```

The last command shown above will serve your static site on the port **5000**. Like many of [serve](https://github.com/zeit/serve)’s internal settings, the port can be adjusted using the `-p` or `--port` flags.

Run this command to get a full list of the options available:

```
serve -h
```

## IDE setup for StandardJS
Chances are your IDE will have a plugin for StandardJS. The the list [here](https://standardjs.com/index.html#are-there-text-editor-plugins)


## IDE setup for Hot Module Replacement

Remember to disable [safe write](https://webpack.github.io/docs/webpack-dev-server.html#working-with-editors-ides-supporting-safe-write) if you are using VIM or IntelliJ IDE, such as WebStorm.



[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)
