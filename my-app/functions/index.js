const functions = require('firebase-functions')
const google = require('googleapis')
const admin = require('firebase-admin')
const cors = require('cors')({
  origin: true
})
const config = functions.config()
const app = admin.initializeApp(config.firebase)

exports.generateAccessToken = functions.https.onRequest((req, resp) => {
    // Enable CORS using 'cors' express middleware
  cors(req, resp, () => {
    const googleJWTClient = new google.auth.JWT(
      // Change to your own value. Key: client_email
      'create-mace-app@create-mace-app-a0374.iam.gserviceaccount.com',
      null,
      // Change to your own value. Key: private_key
      '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC0pm9g2SU/kVIH\nuKVh6ChBXOQmKYMldJBYoP1Esc7jHW0W4ShQR4GBMP1+o2A1V51xnGiSjyiT8E2L\nTIcH8SZCe73PF9T00suKY39o/TZFSZcixiGHFHM3Qg5zq6wChdbix2zBBMwFH79G\nzW5O6k2t8xZvkrgaJUTRgEbcSso89Dmj0b7BGzYhctBRUCXBONXveTBcFBr0oV68\nRlQXPjXDXEqwlJ2YVKy06Pt1pAe+N2q906KtQX8sZo/0VhnCth9eiqDfLz54d3gG\nLzfWfuNOguqElHKRL79Y/TY91I5l/oCofukvKzcIxXZteYkDkGDcE7Ax3NviZcUz\nYRLYuDCVAgMBAAECggEAAnC9JccehF1SM7Mq3h0PbifzF7uNKf8JHtQN7SpPFoJZ\n7D/ea4yLvf22GZ/bYjJszhFu2DasNai+uMMFOqDnWL6TMdkAkiNxhyJmS491duTS\nRdtSA0vOF/usdk47ZKKqOdC33Lj+iJMv4k+kIyzHH90eJc5gQ6TSk9dHyHIPiViS\nulWhtYJZ040skBqtRyzRAQ+FY9IpOpz6Fywhia2aFS6NL+7LPZsrGArsIqhOEmJg\n23/vfLS3Hocf6l7xoq3Ods1Zmv6Z2hPOtX2rnhyhRR0whKy3DajJo2EgxtufoH0S\n52iEbkn2ZO1S975DIzOX4xJNzhCBxocjfAiO3HGlgQKBgQD4nKIbLqmrcE1y5F30\nFV17abkAti4Zrben99dLR6wTpJvycWs3B9cO5WmKwBYOWTNd8idRPPMA1ExA3sZ7\n/MS6H0TsI2uUhicPlGdlmzfepecOnXoRMxsIbHR/8SwA0Ov5gTz1g7o7m31Cd34e\ng7T6vNK8JsWmboGU6isGSAjmLwKBgQC6BMTUyMmjM7+MekY6Y4pWgoO370gDwH3x\ntkwhWRKWhR9GLdFZudF2U+1Er6TlVwGSCSFuKzxscUkGK3suRI/oioG7ktZPiJTO\nn4zl9QmCnLeIFisJjGRW4Cr67hW2zYtg2GOKYFb0CYIQkWKt7n00Yz8oXtscSTen\nqG2AUtnoewKBgQD4ZUdycUUFDcfUK2fUICSu0gsOMvgMoA0HvhFWHd5IIgfOTJQa\n/7JYJ88z2ogu/eDc1L2D5anGAlfN2Mfnyb2lWOi8Jwet9fvBDT3hvxwrUQdzCtST\nqsQvvm0ICCc3CYc8W2xUwbCpLcoRRuK0ClQg0dicNQD2MSM3bmBefkql8QKBgEWq\nQ4ztJZbZaoNRx7tWTBjaCDp6gp3lGUXM3/YdRbjZYj0GK8FOtgN6qwFuRnMdgpkO\nklaQGrECPDbzUc6iHsMYHD4DX/+pRxGa70mv2z/vMUzmAHgy++ENUwtm3vr3P5ul\nbqIEMoxygyNxT2OUTY+xG5Yi8CxHv3Aqw9erL7DBAoGBAJ5jq7fWdnCaYDtp9aDP\nuQTjkdmFp9huetXrkkv+b5UaR74gsJcsmFqx5bqTB2cPXLoVFj7yOtAGMKHE6TtP\nOXUwPJi+WwnmRLClTk4zYZNJz36Ng8ydsxa5Vg0VIR5XG4Ta+zdA2M6VI1EBm+Xb\ntbwV/JhVJRsBpeil3V6Qnvi0\n-----END PRIVATE KEY-----\n',
      // Scope should match your Service Account enabled APIs
      [
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/admin.directory.group.member.readonly',
        'https://www.googleapis.com/auth/admin.directory.group.readonly'
      ],
      // Change to your own value. Email to impersonate.
      'admin@macedev.site'
    )

    googleJWTClient.authorize((error, accessToken) => {
      if (error) {
        console.error("Couldn't generate access token", error)
        return resp.send(
          {
            tokenGenerated: false,
            error: error
          })
      } else {
        saveToken(accessToken)
          .then(() => resp.send(
            {
              tokenGenerated: true,
              tokenSaved: true,
              expiryDate: accessToken.expiry_date
            }
          ))
          .catch(error => resp.send(
            {
              tokenGenerated: true,
              tokenSaved: false,
              expiryDate: accessToken.expiry_date,
              error: error
            }
          ))
      }
    })
  })
})

function saveToken (tokenObj) {
  return new Promise((resolve, reject) => {
    const tokenRef = app.database().ref('/token')
    tokenRef.set(tokenObj)
      .then(function () {
        resolve({
          tokenSaved: true
        })
      })
      .catch(function (error) {
        reject({
          tokenSaved: false,
          error: error
        })
      })
  })
}
