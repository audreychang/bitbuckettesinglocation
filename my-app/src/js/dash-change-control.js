'use strict'
const GoogleChartFactory = require('./fact-google-chart')
const CommentaryFactory = require('./fact-commentary')
const utils = require('./dash-utils')

let changeControlDashboard = function (_project, _data) {
  let crfObj = {}
  let cnObj = {}
  let ccnObj = {}
  let commentaryObj = {}
  let changeControlCrf,
    changeControlCn,
    changeControlContractorCN,
    changeControlCommentary

  renderSection()

  function renderSection () {
        // Replace "" for 0 so the chart will not break
    [_data['Change Control - CN Status'], _data['Change Control - Contractor CN Status']]
      .forEach(function (data) {
        data.forEach(function (row, rowIdx) {
          row.forEach(function (cell, cellIdx) {
            if (cell === '') {
              data[rowIdx][cellIdx] = 0
            }
          })
        })
      })

    crfObj = {
      barColumnChart: 'ColumnChart',
      dataSource: utils.filterByProject(_project, _data['Change Control - CRFs']),
      workstream: _project,
      containerDivId: 'change-control-bar-chart-1',
      workstreamColumn: 2,
      viewColumns: [0, 1, {
        sourceColumn: 1,
        role: 'annotation'
      }],
      annotationColumn: 1,
      isStacked: false,
      legendPosition: 'none',
      annotationOutside: false,
      seriesSettings: {
        0: {
          color: '#42BDEF'
        },
        1: {
          color: '#FF5454'
        }
      },
      chartAreaSettings: {
        left: 120,
        bottom: 50,
        top: 10
      },
      predicateFunction: function (arr) {
        return arr[1] === '0'
      },
      convertColumns: [{
        colIndex: 1,
        dataType: 'number'
      }]
    }

    cnObj = {
      dataSource: utils.filterByProject(_project, _data['Change Control - CN Status']),
      workstream: _project,
      containerDivId: 'change-control-bar-chart-2',
      workstreamColumn: 4,
      viewColumns: [0, 1, {
        sourceColumn: 1,
        role: 'annotation'
      }, 2, {
        sourceColumn: 2,
        role: 'annotation'
      }],
      annotationColumn: null,
      isStacked: true,
      legendPosition: 'bottom',
      annotationOutside: false,
      seriesSettings: {
        0: {
          color: '#42BDEF'
        },
        1: {
          color: '#FF5454'
        }
      },
      predicateFunction: function (arr) {
        return arr[1] === '0' && arr[2] === '0'
      },
      convertColumns: [{
        colIndex: 1,
        dataType: 'number'
      },
      {
        colIndex: 2,
        dataType: 'number'
      }
      ]
    }

    ccnObj = {
      dataSource: utils.filterByProject(_project, _data['Change Control - Contractor CN Status']),
      workstream: _project,
      containerDivId: 'change-control-bar-chart-3',
      workstreamColumn: 4,
      viewColumns: [0, 1, {
        sourceColumn: 1,
        role: 'annotation'
      }, 2, {
        sourceColumn: 2,
        role: 'annotation'
      }],
      annotationColumn: null,
      isStacked: true,
      legendPosition: 'bottom',
      annotationOutside: false,
      seriesSettings: {
        0: {
          color: '#42BDEF'
        },
        1: {
          color: '#FF5454'
        }
      },
      predicateFunction: function (arr) {
        return arr[1] === '0' && arr[2] === '0'
      },
      convertColumns: [{
        colIndex: 1,
        dataType: 'number'
      },
      {
        colIndex: 2,
        dataType: 'number'
      }
      ]
    }

    commentaryObj = {
      dataSource: utils.filterByProject(_project, _data['Change Control - Commentary']),
      workstream: _project,
      containerDivId: 'change-control-commentary-1',
      textAlign: 'justify'
    }

    changeControlCrf = GoogleChartFactory(crfObj)
    changeControlCrf.drawBarChart()

    changeControlCn = GoogleChartFactory(cnObj)
    changeControlCn.drawBarChart()

    changeControlContractorCN = GoogleChartFactory(ccnObj)
    changeControlContractorCN.drawBarChart()

    changeControlCommentary = CommentaryFactory(commentaryObj)
    changeControlCommentary.drawCommentary()
  }
}

module.exports = changeControlDashboard
