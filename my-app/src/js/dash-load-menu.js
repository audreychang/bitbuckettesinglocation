'use strict'

const loadMenuDashboard = function (_data) {
  let ul = $('#menu-ul')
  let $reportingPeriodText,
    reportingPeriodData

  renderSection()

  function renderSection () {
    const forEachIdx = R.addIndex(R.forEach)
    $reportingPeriodText = $('#reporting-period-text')
    reportingPeriodData = R.last(_data['Reporting Period'])

    R.compose(
      forEachIdx(addProjectToDropdown),
      R.map(R.head), // Only the first element of the array.
      R.tail() // No header
    )(_data['Projects'])

    $reportingPeriodText.html(reportingPeriodData)
  }

  function addProjectToDropdown (project, idx) {
    ul.append('<li class="workstream-menu" id="workstream-' + idx + '" data-workstream="' + project + '"><a href="#">' + project + '</a></li>')
  }
}

module.exports = loadMenuDashboard
