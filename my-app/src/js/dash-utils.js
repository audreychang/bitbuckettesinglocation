'use strict'
const DASHBOARDUTILS = (function ($) {
  let applyLoadingOverlay,
    baseAdminUrl,
    baseSheetUrl,
    checkUserPermission,
    convertToJson,
    filterByProgramme,
    filterByProject,
    filterByWbs,
    filterData,
    generalErrorMsg,
    getAccessToken,
    getColIdxByName,
    getData,
    loadGoogleApis,
    notAllowedMsg,
    removeLoadingOverlay,
    setCopyrightYear,
    setSelectedProject,
    showInitialFragment

  baseAdminUrl = function () {
    return 'https://www.googleapis.com/admin/directory/v1/'
  }

  baseSheetUrl = function () {
    return 'https://sheets.googleapis.com/v4/spreadsheets/'
  }

  checkUserPermission = function (approvedDomainList, approvedGroups, user, token, fbApp) {
    return new Promise(function (resolve, reject) {
      let isInDomain, allowed

      isInDomain = belongsToDomain(approvedDomainList, user)

      // TODO: Apply your own check here
      if (approvedGroups.length > 0) {
        belongsToGroup(approvedGroups, user, token.token)
          .then(isInGroup => {
            isInDomain || isInGroup ? allowed = true : allowed = false
            if (allowed) {
              resolve(token)
            } else {
              fbApp.removeFirebaseDBListener()
              reject({
                error: 'Not Allowed',
                message: "Sorry but you can't access this application"
              })
            }
          })
      } else {
        if (isInDomain || approvedDomainList.length === 0) {
          resolve(token)
        } else {
          fbApp.removeFirebaseDBListener()
          reject({
            error: 'Not Allowed',
            message: "Sorry but you can't access this application"
          })
        }
      }
    })
  }

  convertToJson = function (sheetData) {
    let data = []
    let header = R.head(sheetData)
    let rows = R.tail(sheetData)

    rows.forEach(row => {
      let record = {}
      header.forEach((title, idx) => {
        record[title] = convertBool(row[idx])
      })
      data.push(record)
    })

    return data
  }

  filterData = R.curry(
    function (colName, criteria, data) {
      let collectionType = R.type(R.head(data))
      let filteredData,
        colIdx

      if (collectionType === 'Array') {
        if (R.isEmpty(criteria)) {
          return data
        } else {
          colIdx = R.compose(R.indexOf(colName), R.head())(data)
          filteredData = R.filter(arr => arr[colIdx] === criteria || arr[colIdx] === colName)(data)
        }
      }

      if (collectionType === 'Object') {
        if (R.isEmpty(criteria)) {
          return data
        } else {
          filteredData = R.filter(obj => obj[colName] === criteria || obj[colName] === colName)(data)
        }
      }

      if (filteredData.length > 1 || (collectionType === 'Object' && filteredData.length > 0)) {
        return filteredData
      } else {
        return [R.head(data)]
      }
    }
  )

  filterByProject = filterData('Project')
  filterByWbs = filterData('WBS')
  filterByProgramme = filterData('Programme')

  generalErrorMsg = function (error, contactEmail) {
    $('body')
      .html(`
        <div class='text-center'>
          <h1>${error.message}</h1>
          <h1>
            If it persists contact ${contactEmail} with the following message:
          </h1>
        </div>
      `)
  }

  /*
  * It first checks if access token is in localStorage and is still valid.
  * Otherwise will get the token via Firebase function.
  */
  getAccessToken = function (dashConf) {
    return new Promise(function (resolve, reject) {
      const token = window.localStorage.getItem('access_token')
      const tokenExpiryDate = window.localStorage.getItem('expiry_date')
      const now = new Date().getTime()

      if (token !== null && tokenExpiryDate !== null && token !== 'undefined' && tokenExpiryDate !== 'undefined') {
        if (now > tokenExpiryDate) {
          generateAccessToken(dashConf)
            .then(resp => {
              if (resp.tokenGenerated) {
                // The firebase function that generates tokens will save
                // the token to firebase database.
                // And because of setFirebaseDBListener (in dash-firebase.js)
                // the localStorage gets updated as soon as new token is saved
                // to the database.
                let newToken = window.localStorage.getItem('access_token')
                resolve({ token: newToken })
              } else {
                reject(resp.error)
              }
            })
            .catch(error => reject(error))
        } else {
          resolve({ token })
        }
      } else {
        generateAccessToken(dashConf)
          .then(token => resolve({ token }))
          .catch(error => reject(error))
      }
    })
  }

  /* This function will take [['Col1','Col2'],["foo","bar"],["baz","qux"]]
   * and return {'Col1':0,'Col2:1} (values are the columns indexes).
   */
  getColIdxByName = function (array2D) {
    const forEachIdx = R.addIndex(R.forEach)
    let headersObj = {}

    R.compose(
      forEachIdx(function (item, idx) { headersObj[item] = idx }),
      R.head()
    )(array2D)

    return headersObj
  }

  getData = function (dashConf) {
    return new Promise(function (resolve, reject) {
      gapi.client.sheets.spreadsheets.values.batchGet({
        spreadsheetId: dashConf.spreadSheetId,
        ranges: dashConf.rangeList
      })
        .then(
          resp => {
            resp.result.valueRanges.forEach(range => {
              var rangeName, split, sheetValues

              sheetValues = range.values

              /**
               * Get only the sheet name from
               * "SheetName!A1:C" or
               * "'Sheet Name'!A1:C"
               */
              split = range.range.split('!')
              if (split[0].split("'")[1]) {
                rangeName = split[0].split("'")[1]
              } else {
                rangeName = split[0]
              }

              // If any element inside dashConf.convertToJson is equals
              // to rangeName then convert sheet values to JSON.
              if (dashConf.convertToJson.includes(rangeName)) {
                window._data[rangeName] = convertToJson(sheetValues)
              } else {
                window._data[rangeName] = sheetValues
              }
            })
            resolve()
          },
          resp => {
            reject({
              error: resp.result.error.message,
              message: "Couldn't get data this time."
            })
          }
        )
    })
  }

  /*
   * Loads Google Charts and Google Client apis.
   */
  loadGoogleApis = function (accessToken) {
    return new Promise(function (resolve, reject) {
      gapi.load('client', {
        callback: loadGoogleCharts(),
        onerror: function () { console.log('Cannot load Google Client Api') },
        timeout: 3000,
        ontimeout: function () { console.log('gapi.client could not load in a timely manner!') }
      })

      function loadGoogleCharts () {
        google.charts.load('current', {
          'packages': ['corechart', 'table', 'bar', 'line'],
          'callback': initGapiClient
        })
      }

      function initGapiClient () {
        gapi.client.init({
          discoveryDocs: [
            'https://sheets.googleapis.com/$discovery/rest?version=v4',
            'https://www.googleapis.com/discovery/v1/apis/admin/directory_v1/rest',
            'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest'
          ]
        })
          .then(() => {
            gapi.client.setToken({ access_token: accessToken.token })
            resolve()
          }, error => reject(error))
      }
    })
  }

  notAllowedMsg = function (error, callback) {
    $('body')
      .html(`
        <div class='text-center'>
          <h1>${error.message}</h1>
          <button class="btn btn-success" id="auth-btn">
            Try with different account
          </button>
        </div>
      `)
    $('#auth-btn').on('click', function (e) {
      e.preventDefault()
      if (typeof callback === 'function') {
        callback()
      }
    })
  }

  setCopyrightYear = function (elemClass) {
    $('.' + elemClass).text(new Date().getUTCFullYear())
  }

  setSelectedProject = function (project) {
    $('.workstream-dropdown-title').html(project + ' <span class="caret"></span>')
  }

  showInitialFragment = function (fragmentId) {
    $(fragmentId).show()
    $('.dashboard-div:not(' + fragmentId + ')').hide()
  }

  /*
  * APPLY/REMOVE LOADING OVERLAY WHEN PAGE IS LOADING
  * DEPENDS ON LOADINGOVERLAY.JS LIBRARY
  */
  applyLoadingOverlay = function () {
    $.LoadingOverlay('show', {
      image: '',
      fontawesome: 'fa fa-circle-o-notch fa-spin',
      zIndex: 9999
    })
  }

  removeLoadingOverlay = function () {
    $.LoadingOverlay('hide')
  }

  /* ############################################################### */

  /* Helper Functions.
   * They aren't exposed.
   * Only used inside DASHBOARDUTILS.
   */

  function convertBool (value) {
    if (value === 'true') return true
    if (value === 'false') return false
    return value
  }

  function generateAccessToken (dashConf) {
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: dashConf.tokenGeneratorUrl,
        success: resp => {
          // window.localStorage.setItem('access_token', tokenObj.access_token)
          // window.localStorage.setItem('expiry_date', tokenObj.expiry_date)
          // resolve(tokenObj.access_token)
          resolve(resp)
        },
        error: (req, status, error) => (
          reject({
            req,
            status,
            error,
            message: "Couldn't get access token this time."
          })
        )
      })
    })
  }

  function belongsToDomain (domainList, user) {
    const userDomain = R.compose(
      R.last(),
      R.split('@')
    )(user.email)
    if (domainList.includes(userDomain)) {
      return true
    }
    return false
  }

  function belongsToGroup (groupList, user, token) {
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: baseAdminUrl() + 'groups?userKey=' + user.email + '&access_token=' + token,
        success: resp => {
          if (resp.groups) {
            resolve(
              R.compose(
                R.flip(R.gt)(0), // check if size is greater then 0 (user belongs to a group)
                R.length(), // size of the result from intersection
                R.intersection(groupList), // groupList n result_from_map = [] or ['emailN']
                R.map(group => group.email) // ['emai1', 'email2', 'emailN']
              )(resp.groups)
            )
          } else {
            resolve(false)
          }
        },
        error: (req, status, error) => {
          reject({
            req,
            status,
            error,
            message: 'Error here'
          })
        }
      })
    })
  }

  return {
    applyLoadingOverlay,
    baseSheetUrl,
    checkUserPermission,
    convertToJson,
    filterByProgramme,
    filterByProject,
    filterByWbs,
    filterData,
    generalErrorMsg,
    getAccessToken,
    getColIdxByName,
    getData,
    loadGoogleApis,
    notAllowedMsg,
    removeLoadingOverlay,
    setCopyrightYear,
    setSelectedProject,
    showInitialFragment
  }
}($))

module.exports = DASHBOARDUTILS
