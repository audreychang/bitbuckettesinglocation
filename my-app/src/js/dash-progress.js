'use strict'
const DataTableFactory = require('./fact-data-table')
const CommentaryFactory = require('./fact-commentary')
const utils = require('./dash-utils')

let progressDashboard = function (_project, _data) {
  let objConfigKeyAchievements = {}
  let objConfigGoals = {}
  let objConfigProjectSummary = {}
  let progressKeyAchievements,
    progressGoals,
    projectSummary

  renderSection()

  function renderSection () {
    objConfigKeyAchievements = {
      dataSource: utils.filterByProject(_project, _data['Progress - Key Achievements']),
      containerDivId: 'progress-data-table-1',
      workstream: _project,
      workstreamColumn: 1,
      viewColumns: [0],
      headerRowClass: 'hide-table-header',
      tableCellClass: 'table-cell',
      hoverTableRowClass: 'dark-hover',
      oddTableRowClass: 'light-background',
      showRowNumber: true
    }

    objConfigGoals = {
      dataSource: utils.filterByProject(_project, _data['Progress - Goals']),
      containerDivId: 'progress-data-table-2',
      workstream: _project,
      workstreamColumn: 1,
      viewColumns: [0],
      headerRowClass: 'hide-table-header',
      tableCellClass: 'table-cell',
      hoverTableRowClass: 'dark-hover',
      oddTableRowClass: 'light-background',
      showRowNumber: true

    }

    objConfigProjectSummary = {
      dataSource: utils.filterByProject(_project, _data['Project Summary']),
      workstream: _project,
      containerDivId: 'project-summary-1',
      textAlign: 'justify'
    }

    progressKeyAchievements = DataTableFactory(objConfigKeyAchievements)
    progressKeyAchievements.drawDataTable()

    progressGoals = DataTableFactory(objConfigGoals)
    progressGoals.drawDataTable()

    projectSummary = CommentaryFactory(objConfigProjectSummary)
    projectSummary.drawCommentary()
  }
}

module.exports = progressDashboard
