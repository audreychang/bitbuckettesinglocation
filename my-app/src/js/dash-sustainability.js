'use strict'
const DataTableFactory = require('./fact-data-table')
const CommentaryFactory = require('./fact-commentary')
const utils = require('./dash-utils')

let sustainabilityDashboard = function (_project, _data) {
  let objConfigMetric = {}
  let objConfigCommentary = {}
  let sustainabilityMetric,
    sustainabilityCommentary

  renderSection()

  function renderSection () {
    objConfigMetric = {
      dataSource: utils.filterByProject(_project, _data['Sustainability - Metric']),
      containerDivId: 'sustainability-data-table-1',
      workstream: _project,
      workstreamColumn: 2,
      viewColumns: [0, 1],
      headerRowClass: 'soft-table-header',
      headerCellClass: 'dark-border',
      tableCellClass: 'table-cell',
      counterContainerId: 'issue-counter',
      hoverTableRowClass: 'dark-hover',
      useCounter: false,
      showRowNumber: false,
      oddTableRowClass: 'light-background'
    }

    objConfigCommentary = {
      dataSource: utils.filterByProject(_project, _data['Sustainability - Commentary']),
      workstream: _project,
      containerDivId: 'sustainability-commentary-1',
      textAlign: 'justify'
    }

    sustainabilityMetric = DataTableFactory(objConfigMetric)
    sustainabilityMetric.drawDataTable()

    sustainabilityCommentary = CommentaryFactory(objConfigCommentary)
    sustainabilityCommentary.drawCommentary()
  }
}

module.exports = sustainabilityDashboard
