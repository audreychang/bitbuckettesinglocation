'use strict'

let topIndicatorsDashboard = function (_project, _data) {
  let $indicatorImpactJob1 = $('#impact-job1-counter')
  let $indicatorStatus = $('#status-counter')
  let $indicatorRiskOpp = $('#swrm-counter')
  let $indicatorApprovedChanges = $('#swrm-change-counter')

  renderSection()

  function renderSection () {
    _data['Top 4 Indicators'].filter(function (indicator) {
      if (indicator[10] && indicator[10] === _project) {
        var impactJob1 = +indicator[0].trim()
        if (impactJob1 < 0) {
          $indicatorImpactJob1.text(indicator[0].trim() + ' weeks (delay)' || 'NDA')
        } else {
          $indicatorImpactJob1.text(indicator[0].trim() + ' weeks (float)' || 'NDA')
        }
        $indicatorStatus.text((indicator[1].trim() || 'NDA')) // [1] for commercial Target, [2] Baseline (old BDR Status)
        $indicatorRiskOpp.text(indicator[2].trim() || 'TBD') // [2] Baseline, [7] old Risk & Opportunity
        $indicatorApprovedChanges.text(indicator[3].trim() || 'NDA') // [3] Forecast (old Approved Change)
      }
    })
  }
}

module.exports = topIndicatorsDashboard
