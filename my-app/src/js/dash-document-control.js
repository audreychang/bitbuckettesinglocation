'use strict'
const GoogleChartFactory = require('./fact-google-chart')
const CommentaryFactory = require('./fact-commentary')
const utils = require('./dash-utils')

let documentControlDashboard = function (_project, _data) {
  let objConfigDocsDrawings = {}
  let objConfigDocsDrawingsApproval = {}
  let objConfigCommentary = {}
  let objConfigRFIRaised = {}
  let objConfigRFICommentary = {}
  let documentControlDocsDrawings,
    documentControlDocsDrawingsApproval,
    documentControlCommentary,
    documentControlRFIRaised,
    documentControlRFICommentary

  renderSection()

  function renderSection () {
    objConfigDocsDrawings = {
      dataSource: utils.filterByProject(_project, _data['Document Control - Docs and Drawings']),
      workstream: _project,
      containerDivId: 'document-control-bar-chart-1',
      workstreamColumn: 2,
      viewColumns: [0, 1, {
        sourceColumn: 'Value',
        calc: convertCol1ToNumber,
        type: 'number',
        role: 'annotation'
      }],
      isStacked: false,
      legendPosition: 'none',
      seriesSettings: {
        0: {
          color: '#42BDEF'
        },
        1: {
          color: '#FF5454'
        }
      },
      chartAreaSettings: {
        top: 10,
        bottom: 50,
        left: 120
      },
      predicateFunction: function (arr) {
        return arr[1] === '' || arr[1] === '0'
      },
      convertColumns: [{
        colIndex: 1,
        dataType: 'number'
      }]
    }

    objConfigDocsDrawingsApproval = {
      barColumnChart: 'ColumnChart',
      dataSource: utils.filterByProject(_project, _data['Document Control - Docs and Drawings Approval Sum']),
      workstream: _project,
      containerDivId: 'document-control-bar-chart-2',
      workstreamColumn: 2,
      viewColumns: [0, 1, {
        sourceColumn: 1,
        calc: convertCol1ToNumber,
        type: 'number',
        role: 'annotation'
      }],
      isStacked: false,
      legendPosition: 'none',
      seriesSettings: {
        0: {
          color: '#42BDEF'
        },
        1: {
          color: '#FF5454'
        }
      },
      chartAreaSettings: {
        top: 10,
        bottom: 50,
        left: 120
      },
      predicateFunction: function (arr) {
        return arr[1] === '' || arr[1] === '0'
      },
      convertColumns: [{
        colIndex: 1,
        dataType: 'number'
      }]
    }

    objConfigCommentary = {
      dataSource: utils.filterByProject(_project, _data['Document Control - Docs and Drawings Commentary']),
      workstream: _project,
      containerDivId: 'document-control-commentary-1',
      textAlign: 'justify'
    }

    objConfigRFIRaised = {
      barColumnChart: 'ColumnChart',
      dataSource: utils.filterByProject(_project, _data['Document Control - RFI Raised']),
      workstream: _project,
      containerDivId: 'document-control-bar-chart-5',
      workstreamColumn: 2,
      viewColumns: [0, 1, {
        sourceColumn: 1,
        calc: convertCol1ToNumber,
        type: 'number',
        role: 'annotation'
      }],
            // annotationColumn: 1,
      isStacked: false,
      annotationOutside: false,
      legendPosition: 'none',
      seriesSettings: {
        0: {
          color: '#42BDEF'
        },
        1: {
          color: '#FF5454'
        }
      },
      chartAreaSettings: {
        top: 10,
        bottom: 50
      },
      predicateFunction: function (arr) {
        return arr[1] === '' || arr[1] === '0'
      },
      convertColumns: [{
        colIndex: 1,
        dataType: 'number'
      }]
    }

    objConfigRFICommentary = {
      dataSource: utils.filterByProject(_project, _data['Document Control - RFI Commentary']),
      workstream: _project,
      containerDivId: 'document-control-commentary-3',
      textAlign: 'justify'
    }

    function convertCol1ToNumber (dataTable, rowNum) {
      var value = dataTable.getValue(rowNum, 1)
      var number = +value
      return +number.toFixed(0)
    }

    documentControlDocsDrawings = GoogleChartFactory(objConfigDocsDrawings)
    documentControlDocsDrawings.drawBarChart()

    documentControlDocsDrawingsApproval = GoogleChartFactory(objConfigDocsDrawingsApproval)
    documentControlDocsDrawingsApproval.drawBarChart()

    documentControlCommentary = CommentaryFactory(objConfigCommentary)
    documentControlCommentary.drawCommentary()

    documentControlRFIRaised = GoogleChartFactory(objConfigRFIRaised)
    documentControlRFIRaised.drawBarChart()

    documentControlRFICommentary = CommentaryFactory(objConfigRFICommentary)
    documentControlRFICommentary.drawCommentary()
  }
}

module.exports = documentControlDashboard
