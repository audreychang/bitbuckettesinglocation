'use strict'
import './lib-dhtmlxgantt'
import './lib-dhtmlxgantt_marker'
import './lib-dhtmlxgantt_tooltip'

let KeyMilestonesDashboardFactory = function (objConfig) {
  let tasks = {}
  let today = new Date()
  let filteredData,
    ganttDiv,
    keyMilestonesGantt,
    formatDate

  keyMilestonesGantt = Gantt.getGanttInstance()

  filteredData = objConfig.dataSource
  ganttDiv = objConfig.containerDivId
  tasks = {
    data: filteredData
  }
  formatDate = keyMilestonesGantt.date.date_to_str('%d/%m/%Y')

  keyMilestonesGantt.config.grid_width = 500
  // keyMilestonesGantt.config.scale_unit = "quarter"; // Default library quarter
  setScaleConfig('4') // Customised quarter
  keyMilestonesGantt.config.task_height = 15
  keyMilestonesGantt.config.row_height = 40
  keyMilestonesGantt.config.readonly = true

  keyMilestonesGantt.config.columns = [{
    name: 'Activity ID',
    label: 'ID',
    width: 140,
    align: 'left'
  },
  {
    name: 'Activity Name',
    label: 'Activity Name',
    width: '*',
    resize: true,
    align: 'left'
  }
  ]

  keyMilestonesGantt.templates.task_text = function (start, end, task) {
    return ''
  }
  keyMilestonesGantt.templates.tooltip_text = function (start, end, task) {
    task.variance = task.variance ? task.variance : 0
    return '<b>Variance(WW): </b>' + task['variance']
  }

  keyMilestonesGantt.addMarker({
    start_date: today
    // css: "today-marker",
    // text: "Today",
    // title: "Today: " + date_to_str(today)
  })
  keyMilestonesGantt.templates.grid_row_class = function (start, end, task) {
    if (task.variance <= -1) { // Set Activity to red if float is negative
      return 'no-color'
    } else if (task.status === 'Completed') { // Set Activity to green if completed
      return 'completed-green'
    } else {
      return ''
    }
  }

  // Add text next to the milestone forecast diamond
  keyMilestonesGantt.templates.rightside_text = function (start, end, task) {
    if (task.type === keyMilestonesGantt.config.types.milestone) {
      return '(Forecast: ' + formatDate(new Date(task.start_date)) + ')'
    }
    return ''
  }

  /* Add extra element to activity timeline (baseline diamond) */
  // 1 - Convert added data properties to Date objects
  keyMilestonesGantt.attachEvent('onTaskLoading', function (task) {
    task.baseline_end = keyMilestonesGantt.date.parseDate(task.baseline_end, 'xml_date')
    return true
  })

  // 2 - Display custom elements for the planned time
  keyMilestonesGantt.addTaskLayer(function draw_planned (task) {
    let sizes, elWrapper, diamond, diamondTextContainer, text

    if (task.baseline_end) {
      sizes = keyMilestonesGantt.getTaskPosition(task, task.baseline_end)
      elWrapper = document.createElement('div') // Div to wrap the elements
      diamond = document.createElement('div')
      diamondTextContainer = document.createElement('div')
      text = document.createTextNode('(Baseline: ' + formatDate(task.baseline_end) + ')')

      diamond.className = 'baseline-milestone'
      elWrapper.className = 'baseline-wrapper'
      diamondTextContainer.className = 'baseline-text'

      sizes.width = 15
      elWrapper.style.left = (sizes.left - 7) + 'px'
      diamond.style.left = (sizes.left - 7) + 'px' // Position diamond at the right date point
      diamondTextContainer.style.left = (sizes.left - 7) + 'px'
      elWrapper.style.top = sizes.top + keyMilestonesGantt.config.task_height + 7 + 'px'
      diamond.style.top = sizes.top + keyMilestonesGantt.config.task_height + 7 + 'px'
      diamondTextContainer.style.top = sizes.top + keyMilestonesGantt.config.task_height + 7 + 'px'

      diamondTextContainer.appendChild(text)
      elWrapper.appendChild(diamond)
      elWrapper.appendChild(diamondTextContainer)

      return elWrapper
    }

    return false
  })

  // 3 - Specify the CSS rules for the added elements

  // Customisable Scales
  function setScaleConfig (value) {
    switch (value) {
      case '1':
        keyMilestonesGantt.config.scale_unit = 'day'
        keyMilestonesGantt.config.step = 1
        keyMilestonesGantt.config.date_scale = '%d %M'
        keyMilestonesGantt.config.subscales = []
        keyMilestonesGantt.config.scale_height = 27
        keyMilestonesGantt.templates.date_scale = null
        break
      case '2':
        var weekScaleTemplate = function (date) {
          var dateToStr = keyMilestonesGantt.date.date_to_str('%d %M')
          var endDate = keyMilestonesGantt.date.add(keyMilestonesGantt.date.add(date, 1, 'week'), -1, 'day')
          return dateToStr(date) + ' - ' + dateToStr(endDate)
        }

        keyMilestonesGantt.config.scale_unit = 'week'
        keyMilestonesGantt.config.step = 1
        keyMilestonesGantt.templates.date_scale = weekScaleTemplate
        keyMilestonesGantt.config.subscales = [{
          unit: 'day',
          step: 1,
          date: '%D'
        }]
        keyMilestonesGantt.config.scale_height = 50
        break
      case '3':
        keyMilestonesGantt.config.scale_unit = 'month'
        keyMilestonesGantt.config.date_scale = '%F, %Y'
        keyMilestonesGantt.config.subscales = [{
          unit: 'day',
          step: 1,
          date: '%j, %D'
        }]
        keyMilestonesGantt.config.scale_height = 50
        keyMilestonesGantt.templates.date_scale = null
        break
      case '4':
        function quarterLabel (date) {
          var month = date.getMonth()
          var qNum

          if (month >= 9) {
            qNum = 4
          } else if (month >= 6) {
            qNum = 3
          } else if (month >= 3) {
            qNum = 2
          } else {
            qNum = 1
          }
          return 'Q' + qNum
        }

        keyMilestonesGantt.config.subscales = [{
          unit: 'quarter',
          step: 1,
          template: quarterLabel
        },
        {
          unit: 'month',
          step: 1,
          date: '%M'
        }
        ]
        keyMilestonesGantt.config.scale_unit = 'year'
        keyMilestonesGantt.config.step = 1
        keyMilestonesGantt.config.date_scale = '%Y'
        keyMilestonesGantt.config.min_column_width = 50
        keyMilestonesGantt.config.scale_height = 90
        keyMilestonesGantt.templates.date_scale = null
        keyMilestonesGantt.config.start_date = new Date(2016, 2, 1)
        keyMilestonesGantt.config.end_date = new Date(2019, 7, 17)
        break
    }
  }

  function drawGantt () {
    // newGantt(dataSource.keyMilestones);
    // gantt.parse({data: dataSource.keyMilestones});
    keyMilestonesGantt.init(ganttDiv)
    keyMilestonesGantt.parse(tasks)
    return this
  }

  return {
    drawGantt: drawGantt
  }
}

module.exports = KeyMilestonesDashboardFactory
