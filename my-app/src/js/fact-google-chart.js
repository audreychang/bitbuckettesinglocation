'use strict'

let GoogleChartFactory = function (objConfig) {
  let view,
    containerElement,
    bar,
    barOptions,
    dataTable,
    filteredData,
    dateFormatter,
    headers,
    columns

  let defaults = {
    dataSource: undefined,
    workstream: undefined,
    containerDivId: 'bar-chart-1',
    counterContainerId: 'counter-div-1',
    workstreamColumn: 0,
    viewColumns: [1, 2],
    isStacked: false,
    stackTooltip: false,
    legendPosition: 'none',
    seriesType: 'bars',
    seriesSettings: {
      0: {
        color: '#42BDEF'
      },
      1: {
        color: '#FF5454'
      }
    },
    barOrientation: 'horizontal',
    useCounter: false,
    axesSettings: {
      y: {
        0: {
          label: '0'
        }
      }
    },
    barColumnChart: 'BarChart',
    hAxisSettings: {},
    vAxisSettings: {},
    chartAreaSettings: {
      top: 0,
      bottom: 50
    },
    colors: false,
    legendAlignment: 'center',
    annotationOutside: false,
    convertColumns: [],
    pieSliceText: 'value',
    // PredicateFunction needs to return false so R.all can return the final false.
    // R.all returning false means that none of the items in the predcateFunction is true.
    predicateFunction: function () {
      return false
    }
  }

  let settings = Object.assign({}, defaults, objConfig)

  // Filter data based on project selection and convert to dataTable.
  // if (settings.workstreamColumn) {
  //     filteredData = R.filter(arr =>
  //         arr[settings.workstreamColumn] === settings.workstream ||
  //         arr[settings.workstreamColumn] === 'Project'
  //     )(settings.dataSource);
  // } else {
  //     filteredData = settings.dataSource;
  // }
  filteredData = settings.dataSource
  dataTable = new google.visualization.arrayToDataTable(filteredData)

  if (settings.convertColumns.length > 0) {
    settings.convertColumns.forEach(column => {
      changeColDataType(column.colIndex, column.dataType)
    })
  }

  // Create view from dataTable to show specific columns.
  view = new google.visualization.DataView(dataTable)

  headers = getColIdxByName(settings.dataSource)
  columns = R.map(function (item) {
    if (typeof item === 'number') {
      return item
    }
    if (typeof item === 'string') {
      return headers[item]
    }
    if (typeof item === 'object') {
      if (typeof item.sourceColumn === 'number') {
        return item
      }
      if (typeof item.sourceColumn === 'string') {
        const sourceColumnLens = R.lens(R.prop('sourceColumn'), R.assoc('sourceColumn'))
        return R.set(sourceColumnLens, headers[item.sourceColumn], item)
      }
    }
    return item
  }, settings.viewColumns)

  // view.setColumns(settings.viewColumns);
  view.setColumns(columns)

  // Get the div where each chart will be inserted into
  containerElement = document.getElementById(settings.containerDivId)

  // bar = new google.charts.Bar(containerElement); // for material chart
  bar = new google.visualization[settings.barColumnChart](containerElement)
  barOptions = {
    bar: {
      groupWidth: '80%'
    },
    axisTitlesPosition: 'none',
    chartArea: settings.chartAreaSettings,
    animation: {
      startup: true,
      easing: 'in',
      duration: 900
    },
    legend: {
      position: settings.legendPosition,
      alignment: settings.legendAlignment
    },
    axes: settings.axesSettings,
    annotations: {
      alwaysOutside: settings.annotationOutside,
      highContrast: true,
      textStyle: {
        color: '#666666',
        fontSize: 12,
        auraColor: '#00FFFFFF'
      }
    },
    isStacked: settings.isStacked,
    focusTarget: settings.stackTooltip ? 'category' : 'datum',
    hAxis: settings.hAxisSettings,
    vAxis: settings.vAxisSettings,
    width: '100%',
    height: '100%',
    series: settings.seriesSettings,
    pieSliceText: settings.pieSliceText
  }

  dateFormatter = new google.visualization.DateFormat({
    pattern: 'dd/MM/yyyy'
  })

  function changeColDataType (colIndex, dataType) {
    dataTable.insertColumn(colIndex, dataType, dataTable.getColumnLabel(colIndex))
        // copy values from column old column (colIndex + 1) to new column (colIndex), converted to data type
    for (var i = 0; i < dataTable.getNumberOfRows(); i++) {
      var val = dataTable.getValue(i, colIndex + 1)

      if (val && val !== '' && val != null) {
        if (dataType === 'number') {
          dataTable.setValue(i, colIndex, new Number(val).valueOf())
        }
        if (dataType === 'date') {
          let valToArr = val.split('/')
          let [day, month, year] = [...valToArr]
          dataTable.setValue(i, colIndex, new Date(parseInt(year), parseInt(month) - 1, parseInt(day)))
        }
        if (dataType === 'string') {
          dataTable.setValue(i, colIndex, val.toString())
        }
      }
    }
        // remove the old column (colIndex + 1)
    dataTable.removeColumn(colIndex + 1)
    return this
  }

  function convertToNumber (dataTable, rowNum) {
    return +dataTable.getValue(rowNum, 1)
  }

  function getColIdxByName (data) {
    const forEachIdx = R.addIndex(R.forEach)
    let headersObj = {}

    R.compose(
      forEachIdx(function (item, idx) { headersObj[item] = idx }),
      R.head()
    )(data)

    return headersObj
  };

  function applyNumberFormat (options, sourceColumns) {
    let formatter = new google.visualization.NumberFormat(options)
    formatter.format(dataTable, sourceColumns)
    return this
  }

  function printFilteredData () {
    return this
  }

  /*
   * Check for data in each row based on the predicateFunction.
   * This is to show a message that there is no data instead of an empty chart.
   */
  function hasNoData (predicateFunction, projectColIdx, currentProject, data) {
    return R.compose(
      R.all(predicateFunction),
      R.tail()
    )(data)
  };

  function drawBarChart () {
    // var noData = hasNoData(settings.predicateFunction, settings.workstreamColumn, settings.workstream, filteredData);
    var noData = hasNoData(settings.predicateFunction, settings.workstreamColumn, settings.workstream, settings.dataSource)

    if (settings.colors) {
      barOptions.colors = settings.colors
    }

    if (!noData && view.getNumberOfRows() > 0) {
            // bar.draw(view, google.charts.Bar.convertOptions(barOptions)); // For material chart
      bar.draw(view, barOptions)
    } else {
      $('#' + settings.containerDivId).text('No data available.')
    }
    return this
  }

  function logData () {
    // console.log(view.getColumnType(0));
    // console.log(view.getColumnType(1));
    // console.log(view.getColumnType(2));
    console.log(view.getNumberOfColumns())

    return this
  }

  return {
    drawBarChart,
    changeColDataType,
    applyNumberFormat,
    printFilteredData,
    logData,
    dateFormatter,
    convertToNumber
  }
}

module.exports = GoogleChartFactory
