'use strict'
import './lib-dhtmlxgantt'
import './lib-dhtmlxgantt_marker'
import './lib-dhtmlxgantt_tooltip'

let ThreeMonthLookaheadDashboardFactory = function (objConfig) {
  let tasks = {}
  let today = new Date()
  let filteredData,
    ganttDiv,
    threeMonthLookaheadGantt,
    formatDate

  threeMonthLookaheadGantt = Gantt.getGanttInstance()

  filteredData = objConfig.dataSource
  ganttDiv = objConfig.containerDivId
  tasks = {
    data: filteredData
  }
  formatDate = threeMonthLookaheadGantt.date.date_to_str('%d/%m/%Y')

  threeMonthLookaheadGantt.config.grid_width = 500
    // threeMonthLookaheadGantt.config.scale_unit = "quarter"; // Default library quarter
  setScaleConfig('4') // Customised quarter
    // utils.ganttSetScaleConfig(threeMonthLookaheadGantt, 4); // This call makes the app slow!!! Why?!
  threeMonthLookaheadGantt.config.task_height = 15
  threeMonthLookaheadGantt.config.row_height = 40
  threeMonthLookaheadGantt.config.readonly = true
  threeMonthLookaheadGantt.config.start_date = new Date(2017, 5, 1) // The start data has to be the PSR month (0 based).
  threeMonthLookaheadGantt.config.end_date = new Date(2018, 2, 17)

  threeMonthLookaheadGantt.config.columns = [{
    name: 'Activity ID',
    label: 'ID',
    width: 140,
    align: 'left'
  },
  {
    name: 'Activity Name',
    label: 'Activity Name',
    width: '*',
    resize: true,
    align: 'left'
  }
  ]

  threeMonthLookaheadGantt.templates.task_text = function (start, end, task) {
    return ''
  }
  threeMonthLookaheadGantt.templates.tooltip_text = function (start, end, task) {
    return task.type === 'milestone' ? 'Milestone' : '<b>Start date: </b>' + formatDate(start) + '<br/><b>End date: </b>' + formatDate(end) // + "<br/><b>Completed: </b>" + (task.progress * 100).toFixed(0) + "%";
  }

    // Add text next to the milestone forecast diamond
  threeMonthLookaheadGantt.templates.rightside_text = function (start, end, task) {
    if (task.type === threeMonthLookaheadGantt.config.types.milestone) {
      return '(Start: ' + formatDate(task.start_date) + ')'
    }
    return ''
  }

    // Customisable Scales
  function setScaleConfig (value) {
    switch (value) {
      case '1':
        threeMonthLookaheadGantt.config.scale_unit = 'day'
        threeMonthLookaheadGantt.config.step = 1
        threeMonthLookaheadGantt.config.date_scale = '%d %M'
        threeMonthLookaheadGantt.config.subscales = []
        threeMonthLookaheadGantt.config.scale_height = 27
        threeMonthLookaheadGantt.templates.date_scale = null
        break
      case '2':
        var weekScaleTemplate = function (date) {
          var dateToStr = threeMonthLookaheadGantt.date.date_to_str('%d %M')
          var endDate = threeMonthLookaheadGantt.date.add(threeMonthLookaheadGantt.date.add(date, 1, 'week'), -1, 'day')
          return dateToStr(date) + ' - ' + dateToStr(endDate)
        }

        threeMonthLookaheadGantt.config.scale_unit = 'week'
        threeMonthLookaheadGantt.config.step = 1
        threeMonthLookaheadGantt.templates.date_scale = weekScaleTemplate
        threeMonthLookaheadGantt.config.subscales = [{
          unit: 'day',
          step: 1,
          date: '%D'
        }]
        threeMonthLookaheadGantt.config.scale_height = 50
        break
      case '3':
        threeMonthLookaheadGantt.config.scale_unit = 'month'
        threeMonthLookaheadGantt.config.date_scale = '%F, %Y'
        threeMonthLookaheadGantt.config.subscales = [{
          unit: 'day',
          step: 1,
          date: '%j, %D'
        }]
        threeMonthLookaheadGantt.config.scale_height = 50
        threeMonthLookaheadGantt.templates.date_scale = null
        break
      case '4':
        function quarterLabel (date) {
          var month = date.getMonth()
          var qNum

          if (month >= 9) {
            qNum = 4
          } else if (month >= 6) {
            qNum = 3
          } else if (month >= 3) {
            qNum = 2
          } else {
            qNum = 1
          }
          return 'Q' + qNum
        }

        threeMonthLookaheadGantt.config.subscales = [{
          unit: 'quarter',
          step: 1,
          template: quarterLabel
        },
        {
          unit: 'month',
          step: 1,
          date: '%M'
        }
        ]
        threeMonthLookaheadGantt.config.scale_unit = 'year'
        threeMonthLookaheadGantt.config.step = 1
        threeMonthLookaheadGantt.config.date_scale = '%Y'
        threeMonthLookaheadGantt.config.min_column_width = 50
        threeMonthLookaheadGantt.config.scale_height = 90
        threeMonthLookaheadGantt.templates.date_scale = null
                //          threeMonthLookaheadGantt.config.start_date = new Date(2017, 2, 17);
                // threeMonthLookaheadGantt.config.end_date = new Date(2018, 12, 31);
        break
    }
  }

  threeMonthLookaheadGantt.addMarker({
    start_date: today
        // css: "today-marker",
        // text: "Today",
        // title: "Today: " + date_to_str(today)
  })

  function drawGantt () {
    threeMonthLookaheadGantt.init(ganttDiv)
    threeMonthLookaheadGantt.parse(tasks)
    return this
  }

  return {
    drawGantt: drawGantt
  }
}

module.exports = ThreeMonthLookaheadDashboardFactory
