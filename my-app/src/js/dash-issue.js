'use strict'
const DataTableFactory = require('./fact-data-table')
const utils = require('./dash-utils')

let issueDashboard = function (_project, _data) {
  let objConfigKeyIssue = {}
  let keyIssues

  renderSection()

  function renderSection () {
    objConfigKeyIssue = {
      dataSource: utils.filterByProject(_project, _data['Key Issues']),
      containerDivId: 'key-issues-data-table-1',
      workstream: _project,
      workstreamColumn: 2,
      viewColumns: [0, 1],
      headerRowClass: 'soft-table-header',
      headerCellClass: 'dark-border',
      tableCellClass: 'table-cell',
      counterContainerId: 'issue-counter',
      hoverTableRowClass: 'dark-hover',
      useCounter: true,
      oddTableRowClass: 'light-background'
    }

    keyIssues = DataTableFactory(objConfigKeyIssue)
    keyIssues.drawDataTable()
  }
}

module.exports = issueDashboard
