'use strict';

const fs = require('fs');
const copySync = require('fs-extra').copySync;
const path = require('path');
const chalk = require('chalk');
const spawnSync = require('child_process').spawnSync;
const argv = require('minimist')(process.argv.slice(2));
const commands = argv._;
const executablePaths = require('elm/platform').executablePaths;

if (commands.length === 0 || commands[0] === '') {
  console.error('\nUsage: mace-app create <project-directory>');
  process.exit(1);
}

createMaceApp(commands[0]);

function createMaceApp(name) {
  console.log('\nCreating ' + name + ' project...\n');

  const root = path.resolve(name);
  const template = path.join(__dirname, '../template');

  if (!fs.existsSync(name)) {
    try {
      copySync(template, root);
      fs.renameSync(
        path.resolve(root, 'gitignore'),
        path.resolve(root, '.gitignore')
      );
    } catch (err) {
      console.log(err);
      process.exit(1);
    }
  } else {
    console.log('The directory ' + name + ' already exists. Aborting.');
    process.exit(1);
  }

  process.chdir(root);
  console.log('Changing directory to ' + process.cwd())
  console.log()

  // Run initial `elm-package install -y`
  const spawnElmPkgResult = spawnSync(
    executablePaths['elm-package'],
    ['install', '-y'],
    { stdio: 'inherit' }
  );

  // Run initial 'npm install'
  console.log()
  console.log('Installing npm packages. This can take some time. Coffee maybe?! :)')
  console.log()
  const spawnNpmPkgResult = spawnSync(
      'npm',
      ['install'],
      { stdio: 'inherit', shell: true }
  );

  if (spawnElmPkgResult.status !== 0) {
    console.log(chalk.red('\nFailed to install elm packages'));
    console.log('\nPlease, make sure you have internet connection!');
    console.log(
      '\nIn case if you are running Unix OS, you might look in to this issue:'
    );
    console.log('\nhttps://github.com/halfzebra/create-mace-app/issues/10');
    process.exit(1);
  }
    
  if (spawnNpmPkgResult.status !== 0) {
    console.log(chalk.red('\nFailed to install npm packages'));
    console.log('\nPlease, make sure you have internet connection!');
    console.log('\n    https://bitbucket.org/maceprogrammecontrols/create-mace-app/issues/10');
    process.exit(1);
  }


  console.log(
    chalk.green('\nProject is successfully created in `' + root + '`.')
  );
  console.log();
  console.log('Inside that directory, you can run several commands:');
  console.log();
  console.log(chalk.cyan('  mace-app start'));
  console.log('    Starts the development server.');
  console.log();
  console.log(chalk.cyan('  mace-app build'));
  console.log('    Bundles the app into static files for production.');
  console.log();
  // console.log(chalk.cyan('  mace-app deploy-functions'));
  // console.log('    Deploys Firebase functions only.');
  // console.log();
  // console.log(chalk.cyan('  mace-app deploy-app'));
  // console.log('    Bundles the app and deploys to Firbase Hosting only.');
  // console.log();
  // console.log(chalk.cyan('  mace-app deploy-all'));
  // console.log('    Bundles the app and deploys to all Firebase services.');
  // console.log();
  // console.log(chalk.cyan('  mace-app test'));
  // console.log('    Starts the test runner.');
  // console.log();
  console.log(chalk.cyan('  mace-app eject'));
  console.log(
    '    Removes this tool and copies build dependencies, configuration files'
  );
  console.log(
    '    and scripts into the app directory. If you do this, you can’t go back!'
  );
  console.log();
  console.log('We suggest you configure your Firebase and Service Account by following the README.');
  console.log('Once everything is setup you can run:');
  console.log();
  console.log(chalk.cyan('  cd'), name);
  console.log('  ' + chalk.cyan('mace-app start'));
  console.log();
}
