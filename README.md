# Create Mace App

Create Mace apps with no build configuration.

* [Getting Started](#markdown-header-getting-started) – How to create a new app.
* [User Guide](https://bitbucket.org/maceprogrammecontrols/create-mace-app) – How to develop apps bootstrapped with Create Mace App.


## Getting Started

### Installation

**Node >=6** is required for installation.


#### Clone Repo
`git clone https://your_user_name@bitbucket.org/maceprogrammecontrols/create-mace-app.git`

#### Change to repo directory:

`cd create-mace-app`

#### Install dependencies:

`npm install`

#### Link repo to global npm so you can use CLI commands:

`npm link`


### Creating an App

To create a new app, run:

```sh
create-mace-app my-app
cd my-app/
```


Creates a new `my-app` folder with files for your future project.

```
my-app/
  .gitignore
  README.md
  elm-package.json
  src/
    html
    js
    css
    favicon.ico
  functions
    index.js
    package.json
  tests/
    Tests.elm
    elm-package.json
```

You are ready to employ the full power of Create Mace App!

### `mace-app start`
Run the app in development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


The page will reload if you make edits.
You will see the build errors and lint warnings in the console and the browser window.

### `mace-app build`
Builds the app for production to the `build` folder.
It bundles Mace app and optimizes the build for the best performance.

The build is minified, and the filenames include the hashes.

**Note**: *Before running `mace-app build` follow the instructions on your project's [README](template/README.md?at=master#markdown-header-before-your-first-deployment).*

## Guide
Every generated project will contain a readme with guidelines for future development. 
The latest version is available [here](template/README.md?at=master)

## Why use this?
This will allow you to create an application that can be used for any Mace development. Fair to say that it is focus on dashboards.

Create Mace App adds a tool for optimizing production builds and running a development server with your app, which supports HMR.

All of that in a single command line tool, which you might use as a dependency.

## What is inside

The tools used by Create Mace App are subject to change.

* [webpack](https://webpack.js.org/) with [webpack-dev-server](https://github.com/webpack/webpack-dev-server), [html-webpack-plugin](https://github.com/ampedandwired/html-webpack-plugin) and [style-loader](https://github.com/webpack/style-loader)
* [Babel](http://babeljs.io/) with ES6 and ES7
* [Autoprefixer](https://github.com/postcss/autoprefixer)
* [StandarJS](https://standardjs.com)
* [Bootstrap](https:getbootstrap.com)
* [JQuery](https://jquery.com)
* [JQuery-UI](https://jqueryui.com)
* [RamdaJS](ramdajs.com)
* [RxJS](reactivex.io)
* [Google API Client Library](https://developers.google.com/api-client-library)
* [Google Visualization](https://developers.google.com/chart/interactive/docs/reference)
* [Firebase Hosting](https://firebase.google.com/docs/hosting)
* [Firebase Functions](https://firebase.google.com/docs/functions)
* [Firebase Authentication](https://firebase.google.com/docs/auth/web/start)
* [Service Account](https://cloud.google.com/iam/docs/service-accounts)
* [DHTMLXGantt](https://docs.dhtmlx.com/gantt)
* [Sheets API](https://developers.google.com/sheets/api)
* [Drive API](https://developers.google.com/drive/v3/web/about-sdk)
* [Admin SDK](https://developers.google.com/admin-sdk)
* [elm-platform](https://github.com/elm-lang/elm-platform)
* [elm-test](https://github.com/elm-community/elm-test)
* and others.

All of them are transitive dependencies of the provided npm package.

## Contributing
We would love to get you involved! Please check our [Contributing Guide](CONTRIBUTING.md) to get started!

## Alternatives
Your own setup.


*Inspired by [create-elm-app](https://github.com/halfzebra/create-elm-app) which was inspired by [create-react-app](https://github.com/facebookincubator/create-react-app)*
