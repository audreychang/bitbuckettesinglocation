'use strict'

const utils = require('./dash-utils')
const DataTableFactory = require('./fact-data-table')

let programmeOnPageDashboard = function (_project, _data) {
  let objConfig = {}
  let programmeOnPageMetric,
    $ragCircle

  renderSection()

  function renderSection () {
    objConfig = {
      dataSource: _data['Programme On a Page'],
      containerDivId: 'programme-on-page-data-table-1',
      workstream: _project,
      viewColumns: [0, 1, 2, 3, 4, 5, 6, 7],
      headerRowClass: 'soft-table-header',
      headerCellClass: 'dark-border rag-header',
      tableCellClass: 'table-cell rag-cell',
      hoverTableRowClass: 'dark-hover',
      useCounter: false,
      showRowNumber: false,
      oddTableRowClass: 'light-background',
      sort: 'disable'
    }

    programmeOnPageMetric = DataTableFactory(objConfig)
    programmeOnPageMetric.drawDataTable()
    applyProgressRag($('#programme-on-page-data-table-1 div div table tbody tr'))

    $ragCircle = $('.rag-circle')
    $ragCircle.each(function (index, elem) {
      let circleConfig = {
        elem: $(elem),
        ragColor: $(elem).data('color'),
        sectionName: $(elem).data('section'),
        project: $(elem).data('project'),
        sectionId: $(elem).data('section-id'),
        tooltipPosition: 'right'
      }

      setupRagCircle(circleConfig)
    })

    function setupRagCircle (circleConfig) {
      let sectionName = circleConfig.sectionName
      let project = circleConfig.project
      let position = circleConfig.tooltipPosition
      let sectionId = circleConfig.sectionId
      let ragColour = circleConfig.ragColor.charAt(0).toUpperCase() + circleConfig.ragColor.slice(1)
      let matrixObj

              /* IE doesn't implement .find() so I'm using Ramda.js .find() function.
               * matrixObj = programmeOnPageMatrix.find(function(item) { return item.Section === sectionName; });
               * Underscore can also be used: _.find(programmeOnPageMatrix, function(item) { return item.Section === sectionName; });
               */
      matrixObj = R.find(R.propEq('Section', sectionName))(_data['Programme On a Page - Matrix'])
      $(circleConfig.elem)
        .attr('data-toggle', 'tooltip')
        .attr('data-placement', position)
        .attr('title', matrixObj[ragColour])
        .on('click', function () {
          $('[data-toggle="tooltip"]').tooltip('close')
          loadDashboardItem(project, sectionId)
        })

      $('[data-toggle="tooltip"]').tooltip()
    }

    function loadDashboardItem (project, sectionId) {
      let secId = sectionId
      utils.setSelectedProject(project)

      window._project = project
      $('.counters-container').removeClass('hidden')
      $('.page-title-box').removeClass('hidden')
      $('.section-btn').removeClass('disable')
      $('.workstream-dropdown-title').html(window._project + ' <span class="caret"></span>')

      $('#programme-on-page-btn > a').removeClass('subdrop')
      $('#' + secId + '-btn').click().find('a').addClass('subdrop')
    };

    function applyProgressRag (tableRows) {
      tableRows.each(function (idx, elem) {
        var projectName = $(R.head($(elem).find('td'))).text()
        var tds = R.tail($(elem).find('td')) // All the tds (without first td = project name) from current tr.
        var progress = R.head(tds) // Progress td (cell) for this tr.
        var metrics = R.tail(tds) // The rest of td for this tr.
        var iList = []
        var rag
        var score

        metrics.each(function (idx, td) { // Loop through each metric (td).
          var i = $(td).find('i') // Find i tag for this td.
          iList.push(i) // Add i to a iList.
        })

        score = R.compose(
          R.sum(), // Peform sum on the array. This will return a single value.
          R.map(function (i) {
            return $(i).data('score')
          }) // Map through each i and return just the score. This will return an array with scores only.
        )(iList)

        if (score < 0) rag = 'red'
        if (score > 0) rag = 'green'
        if (score === 0) rag = 'amber'

        $(progress).html(
                      "<i id='prog-" + idx + "' class='fa fa-circle rag-circle rag-" + rag + "' data-score='" + score + "'data-section='Progress' data-section-id='progress' data-project='" + projectName + "' data-color='" + rag + "' data-toggle='tooltip' data-placement='right'></i>"
                  )
      })
    }
  }
}

module.exports = programmeOnPageDashboard
