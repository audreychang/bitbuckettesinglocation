'use strict'

const firebaseConf = {
  apiKey: 'AIzaSyAHbmq4ADDRPEfiOcW8TCDWs4lQ9pw2nvE',
  authDomain: 'create-mace-app-a0374.firebaseapp.com',
  databaseURL: 'https://create-mace-app-a0374.firebaseio.com',
  projectId: 'create-mace-app-a0374',
  storageBucket: '',
  messagingSenderId: '363130383153'
}

const dashConf = {
  tokenGeneratorUrl: 'https://us-central1-create-mace-app-a0374.cloudfunctions.net/generateAccessToken',
  spreadSheetId: '1DvSMsia2BkEgaFMkpk_S0kSCd-ZNvMFE5D043XttPPU', // Dummy data
  approvedDomains: [],
  approvedGroups: [],
  adminEmail: 'your_admin_email@site.com',
  rangeList: [
    'Projects',
    'Milestones - Commentary',
    'Key Milestones Data',
    '3-Month Lookahead Data',
    'Header Info Data',
    'Programme On a Page - Matrix',
    'Programme On a Page',
    'Progress - Key Achievements',
    'Progress - Goals',
    'Project Summary',
    'Key Issues',
    'Change Control - CRFs',
    'Change Control - CN Status',
    'Change Control - Contractor CN Status',
    'Change Control - Commentary',
    'Commercial - Waterfall',
    'Commercial - Commentary',
    'Document Control - Docs and Drawings',
    'Document Control - Docs and Drawings Approval Sum',
    'Document Control - Docs and Drawings Commentary',
    'Document Control - RFI Raised',
    'Document Control - RFI Commentary',
    'Risks & Opportunities - Exposure',
    'Risks & Opportunities - Top 5 Risks',
    'Risks & Opportunities - Top 5 Opportunities',
    'Risks & Opportunities - RAG Commentary',
    'Safety - Metric',
    'Safety - Commentary',
    'Sustainability - Metric',
    'Sustainability - Commentary',
    'Top 4 Indicators',
    'Reporting Period'
  ],
  convertToJson: [
    'Key Milestones Data',
    '3-Month Lookahead Data',
    'Header Info Data',
    'Programme On a Page - Matrix'
  ]
}

module.exports = {
  firebaseConf,
  dashConf
}
