'use strict'

/* Notes:
 * - Login is done automatically,
 * - There is a listener on the 'token' object in Firebase Database that will update the token on all clients when token is expired and new token is requested.
 * - User is checked whether or not it can access the application.
 *   If not allowed, localStorage is deleted and token listener is disabled.
 * - getData function saves data to global variable window._data.
 *   To access each data set: window._data['Sheet_Name'].
 */

const startApp = function (config, callback) {
  const utils = require('./dash-utils')
  const fb = require('./dash-firebase-app')()
  fb.authenticateUser(window._user)
    .then(() => fb.setFirebaseDBListener())
    .then(() => utils.getAccessToken(config.dashConf))
    .then(accessToken => utils.checkUserPermission(
      config.dashConf.approvedDomains,
      config.dashConf.approvedGroups,
      window._user,
      accessToken,
      fb)
    )
    .then(accessToken => utils.loadGoogleApis(accessToken))
    .then(() => utils.getData(config.dashConf))
    .then(callback)
    .catch(error => {
      if (error.error === 'Not Allowed') {
        fb.removeFirebaseDBListener()
        utils.notAllowedMsg(error, fb.logout)
      } else {
        utils.generalErrorMsg(error, config.dashConf.adminEmail)
      }
    })
}

module.exports = startApp
