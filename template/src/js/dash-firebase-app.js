/* Firebase Settings */
import 'firebase/database'
import 'firebase/auth'

const firebase = require('firebase/app')
const config = require('./dash-config')
const provider = new firebase.auth.GoogleAuthProvider()
provider.setCustomParameters({prompt: 'select_account'})
const fb = firebase.initializeApp(config.firebaseConf)

let rootRef

const firebaseApp = function (elm) {
  rootRef = firebase.database().ref()

    /* Authenticate user */
  function authenticateUser (globalUser) {
    return new Promise(function (resolve, reject) {
      // Listens for sign-in state changes
      fb.auth().onAuthStateChanged(
        function (user) {
          if (user) {
            window._user = user.toJSON()
            resolve(user.toJSON())
          } else {
            authenticate()
          }
        },
        function (error) {
          reject(error)
        })
    })
  }

  function authenticate () {
    fb.auth().signInWithRedirect(provider)
    fb.auth().getRedirectResult().then(function (result) {
      // This gives you a Google Access Token. You can use it to access the Google API.
      // Don't need this token.
      // Access token comes from Service Account JWT setup.
      // var token = result.credential.accessToken;

      // The signed-in user info.
      // var user = result.user;
      // return user;

    }).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code
      var errorMessage = error.message
      // The email of the user's account used.
      var email = error.email
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential

      // TODO: Send error to Elm
      // elm.ports.authError.send(error);
      return {
        errorCode,
        errorMessage,
        email,
        credential
      }
    })
  }

  function deleteUser () {
    return new Promise(function (resolve, reject) {
      const user = fb.auth().currentUser
      user.delete()
        .then(function () { resolve({ userDeleted: true }) })
        .catch(function (e) { reject({ userDeleted: false, error: e }) })
    })
  }

  function logout () {
    fb.auth().signOut()
      .then(function () {
        console.log('I am out')
      })
  }

  function setFirebaseDBListener () {
    return new Promise(function (resolve, reject) {
      // Sets a listener for when access_token changes on Firebase Database
      let pathObj = {
        token: {
          access_token: '',
          expiry_date: '',
          refresh_token: '',
          token_type: ''
        }
      }
      rootRef.on('value', function (snapshot) {
        if (!snapshot.hasChild('token/access_token') && !snapshot.hasChild('token/expiry_date')) {
          rootRef.set(pathObj)
        } else {
          updateLocalStorage(snapshot.val())
          resolve()
        }
      })
    })
  }

  function removeFirebaseDBListener () {
    rootRef.off()
    clearLocalStorage()
  }

  function updateLocalStorage (data) {
    window.localStorage.setItem('access_token', data.token.access_token)
    window.localStorage.setItem('expiry_date', data.token.expiry_date)
  }

  function clearLocalStorage () {
    window.localStorage.removeItem('access_token')
    window.localStorage.removeItem('expiry_date')
  }

  return {
    fb,
    database: firebase.database,
    deleteUser,
    authenticateUser,
    logout,
    setFirebaseDBListener,
    removeFirebaseDBListener
  }
}

module.exports = firebaseApp
