'use strict'
const DataTableFactory = require('./fact-data-table')
const CommentaryFactory = require('./fact-commentary')
const utils = require('./dash-utils')

let safetyDashboard = function (_project, _data) {
  let objConfigMetric = {}
  let objConfigCommentary = {}
  let safetyMetric,
    safetyCommentary

  renderSection()

  function renderSection () {
    objConfigMetric = {
      dataSource: utils.filterByProject(_project, _data['Safety - Metric']),
      containerDivId: 'safety-data-table-1',
      workstream: _project,
      workstreamColumn: 3,
      viewColumns: [0, 1, 2],
      headerRowClass: 'soft-table-header',
      headerCellClass: 'dark-border',
      tableCellClass: 'table-cell',
      counterContainerId: 'issue-counter',
      hoverTableRowClass: 'dark-hover',
      useCounter: false,
      showRowNumber: false,
      oddTableRowClass: 'light-background',
      pageSize: 12
    }

    objConfigCommentary = {
      dataSource: utils.filterByProject(_project, _data['Safety - Commentary']),
      workstream: _project,
      containerDivId: 'safety-commentary-1',
      textAlign: 'justify'
    }

    safetyMetric = DataTableFactory(objConfigMetric)
    safetyMetric.drawDataTable()

    safetyCommentary = CommentaryFactory(objConfigCommentary)
    safetyCommentary.drawCommentary()
  }
}

module.exports = safetyDashboard
