'use strict'

let DataTableFactory = function (objConfig) {
  let table,
    dataTable,
    filteredData,
    containerElement,
    view,
    headers,
    columns

  let defaults = {
    dataSource: undefined,
    containerDivId: 'data-table-1',
    workstream: undefined,
    workstreamColumn: 0,
    viewColumns: [1, 2],
    headerRowClass: '',
    useCounter: false,
    counterContainerId: 'counter-div-1',
    headerCellClass: '',
    sortColumn: -1,
    tableCellClass: '',
    hoverTableRowClass: '',
    showRowNumber: false,
    oddTableRowClass: '',
    tableWidth: '100%',
    tableHeight: '100%',
    allowHtml: true,
    sort: 'enable',
    pageSize: 10
  }

  let settings = Object.assign({}, defaults, objConfig)

  // Filter data based on project selection and convert to dataTable.
  // if (settings.workstreamColumn) {
  //     filteredData = R.filter(arr =>
  //         arr[settings.workstreamColumn] === settings.workstream ||
  //         arr[settings.workstreamColumn] === 'Project'
  //     )(settings.dataSource);
  // } else {
  //     filteredData = settings.dataSource;
  // }

  filteredData = settings.dataSource
  dataTable = new google.visualization.arrayToDataTable(filteredData)

    // Create view for data table to show specific columns.
  view = new google.visualization.DataView(dataTable)

  headers = getColIdxByName(settings.dataSource)
  columns = R.map(function (item) {
    if (typeof item === 'number') {
      return item
    }
    if (typeof item === 'string') {
      return headers[item]
    }
    if (typeof item === 'object') {
      if (typeof item.sourceColumn === 'number') {
        return item
      }
      if (typeof item.sourceColumn === 'string') {
        const sourceColumnLens = R.lens(R.prop('sourceColumn'), R.assoc('sourceColumn'))
        return R.set(sourceColumnLens, headers[item.sourceColumn], item)
      }
    }
    return item
  }, settings.viewColumns)

    // view.setColumns(settings.viewColumns);
  view.setColumns(columns)

    // Get the div where each chart will be inserted into
  containerElement = document.getElementById(settings.containerDivId)

    // Create Table object (using ChartWrapper and setting config/options)
  table = new google.visualization.ChartWrapper({
    chartType: 'Table',
    containerId: containerElement,
    options: {
      width: settings.tableWidth,
      height: settings.tableHeight,
      allowHtml: settings.allowHtml,
      cssClassNames: {
        'oddTableRow': settings.oddTableRowClass,
        'headerRow': settings.headerRowClass,
        'headerCell': settings.headerCellClass,
        'tableCell': settings.tableCellClass,
        'hoverTableRow': settings.hoverTableRowClass
      },
      page: 'enable',
      pageSize: settings.pageSize,
      pagingButtons: 'auto',
      sort: settings.sort,
      sortColumn: settings.sortColumn,
      sortAscending: false,
      alternatingRowStyle: true,
      showRowNumber: settings.showRowNumber
    }
  })

    // Set data for table and pie chart and draw it
  table.setDataTable(view)

  function applyPatternFormat (pattern, sourceColumns, destinationColumn) {
    let formatter = new google.visualization.PatternFormat(pattern)
    formatter.format(dataTable, sourceColumns, destinationColumn)
    return this
  }

  function getColIdxByName (data) {
    const forEachIdx = R.addIndex(R.forEach)
    let headersObj = {}

    R.compose(
        forEachIdx(function (item, idx) { headersObj[item] = idx }),
      R.head()
    )(data)

    return headersObj
  };

  function drawDataTable () {
    table.draw()
    return this
  }

  return {
    drawDataTable: drawDataTable,
    applyPatternFormat: applyPatternFormat
  }
}

module.exports = DataTableFactory
