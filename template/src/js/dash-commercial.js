'use strict'
const GoogleChartFactory = require('./fact-google-chart')
const CommentaryFactory = require('./fact-commentary')
const utils = require('./dash-utils')

let commercialDashboard = function (_project, _data) {
  let objConfigWaterfall = {}
  let objConfigCommentary = {}
  let commercialWaterfall,
    commercialCommentary

  renderSection()

  function renderSection () {
    objConfigWaterfall = {
      barColumnChart: 'ColumnChart',
      dataSource: utils.filterByProject(_project, _data['Commercial - Waterfall']),
      workstream: _project,
      containerDivId: 'commercial-bar-chart-1',
      workstreamColumn: 12, // 14 for old Green Sheet
      viewColumns: [
        0,
        1, {
          sourceColumn: 1,
          role: 'annotation'
        },
        2, {
          sourceColumn: 2,
          role: 'annotation'
        },
        3, {
          sourceColumn: 3,
          role: 'annotation'
        },
        4, {
          sourceColumn: 4,
          role: 'annotation'
        },
        5, {
          sourceColumn: 5,
          role: 'annotation'
        },
        6, {
          sourceColumn: 6,
          role: 'annotation'
        },
        7, {
          sourceColumn: 7,
          role: 'annotation'
        },
        8, {
          sourceColumn: 8,
          role: 'annotation'
        },
        9, {
          sourceColumn: 9,
          role: 'annotation'
        },
        10, {
          sourceColumn: 10,
          role: 'annotation'
        },
        11, {
          sourceColumn: 11,
          role: 'annotation'
        }
                // 12, { sourceColumn: 12, role: "annotation" }, // Uncomment for previous green sheet
                // 13, { sourceColumn: 13, role: "annotation" }, // Uncomment for previous green sheet
      ],
      isStacked: true,
      stackTooltip: true,
      legendPosition: 'none', // 'right' for old Green Sheet
      legendAlignment: 'center',
      annotationOutside: true,
      chartAreaSettings: {
        top: 50,
        bottom: 100
      },
            // colors: ['transparent','#1C4587', '#1155CC', '#3C78D8', '#6D9EEB', '#A4C2F4', '#C9DAF8', '#CFE2F3', '#45818E', '#B4A7D6', '#45818E', '#E06666', '#93C47D', '#45818E'], // uncomment for old Green Sheet
            // colors: ['transparent','#1C4587', '#F6B26B', '#8E7CC3', '#93C47D', '#6FA8DC', '#E06666', '#C9DAF8', '#FCE5CD', '#D9D2E9', '#D9EAD3', '#CFE2F3', '#F4CCCC'],
      colors: [
                // Budget section: blue
        '#A4C2F4', '#3C78D8', '#1C4587',
                // Project Costs section: brown
        '#D5A6BD', '#A64D79', '#4C1130',
                // Total Costs section: yellow
        '#FFE599', '#BF9000',
                // Valuations section: purple
        '#B4A7D6',
                // Best Case section: green
        '#B6D7A8',
                // Worst Case section: red
        '#EA9999'
      ],
      seriesSettings: {
        0: {
          visibleInLegend: false,
          annotations: {
            stem: {
              length: -18
            }
          }
        },
        1: {
          visibleInLegend: false,
          annotations: {
            stem: {
              length: 15
            }
          }
        },
        2: {
          visibleInLegend: false,
          annotations: {
            stem: {
              length: 30
            }
          }
        },
        3: {
          visibleInLegend: false,
          annotations: {
            stem: {
              length: -18
            }
          }
        },
        4: {
          visibleInLegend: false,
          annotations: {
            stem: {
              length: 15
            }
          }
        },
        5: {
          visibleInLegend: false,
          annotations: {
            stem: {
              length: 30
            }
          }
        },
        6: {
          visibleInLegend: false,
          annotations: {
            stem: {
              length: -18
            }
          }
        },
        7: {
          visibleInLegend: false,
          annotations: {
            stem: {
              length: 15
            }
          }
        }
      },
      hAxisSettings: {
        slantedText: false,
        showTextEvery: 1
      },
      vAxisSettings: {
        format: 'short'
      },
      convertColumns: [{
        colIndex: 1,
        dataType: 'number'
      },
      {
        colIndex: 2,
        dataType: 'number'
      },
      {
        colIndex: 3,
        dataType: 'number'
      },
      {
        colIndex: 4,
        dataType: 'number'
      },
      {
        colIndex: 5,
        dataType: 'number'
      },
      {
        colIndex: 6,
        dataType: 'number'
      },
      {
        colIndex: 7,
        dataType: 'number'
      },
      {
        colIndex: 8,
        dataType: 'number'
      },
      {
        colIndex: 9,
        dataType: 'number'
      },
      {
        colIndex: 10,
        dataType: 'number'
      },
      {
        colIndex: 11,
        dataType: 'number'
      }
                // { colIndex: 12, dataType: 'number' } // uncomment for previous Green Sheet
      ]
    }

    objConfigCommentary = {
      dataSource: utils.filterByProject(_project, _data['Commercial - Commentary']),
      workstream: _project,
      containerDivId: 'commercial-commentary-1',
      textAlign: 'justify'
    }

        // let patternOptions = { pattern: '_(* £#,0.00,,"m"_);_(* £(#,0.00,,"m");_(*0 _);_(@_)' };
    let patternOptions = {
      pattern: '€#,###'
    }
    commercialWaterfall = GoogleChartFactory(objConfigWaterfall)
    commercialWaterfall
      .applyNumberFormat(patternOptions, 1)
      .applyNumberFormat(patternOptions, 2)
      .applyNumberFormat(patternOptions, 3)
      .applyNumberFormat(patternOptions, 4)
      .applyNumberFormat(patternOptions, 5)
      .applyNumberFormat(patternOptions, 6)
      .applyNumberFormat(patternOptions, 7)
      .applyNumberFormat(patternOptions, 8)
      .applyNumberFormat(patternOptions, 9)
      .applyNumberFormat(patternOptions, 10)
      .applyNumberFormat(patternOptions, 11)
      .applyNumberFormat(patternOptions, 12) // uncomment for old Green Sheet
      .drawBarChart()

    commercialCommentary = CommentaryFactory(objConfigCommentary)
    commercialCommentary.drawCommentary()
  }
}

module.exports = commercialDashboard
