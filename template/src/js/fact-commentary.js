'use strict'

let CommentaryFactory = function (objConfig) {
  let $containerDivId,
    filteredData,
    text,
    sanitizedText

  let defaults = {
    workstream: undefined,
    dataSource: undefined,
    containerDivId: 'comment-1',
    textAlign: 'justify'
  }

  let settings = Object.assign({}, defaults, objConfig)

  $containerDivId = $('#' + settings.containerDivId)
  filteredData = R.flatten(R.tail(settings.dataSource))

  function drawCommentary () {
    if (filteredData.length > 0) {
      text = filteredData[0]
      sanitizedText = text.replace(/\n/g, function (match, p1, offset, string) {
        return p1 ? '<br>' : match
      })
      $containerDivId.html(sanitizedText).css('text-align', settings.textAlign || 'justify')
    } else {
      $containerDivId.html('No data available').css('text-align', 'center')
    }

    return this
  }

  return {
    drawCommentary: drawCommentary
  }
}

module.exports = CommentaryFactory
