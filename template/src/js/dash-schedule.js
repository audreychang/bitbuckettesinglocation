'use strict'
const KeyMilestonesDashboardFactory = require('./fact-key-milestones')
const ThreeMonthLookaheadDashboardFactory = require('./fact-3month-lookahead')
const CommentaryFactory = require('./fact-commentary')
const utils = require('./dash-utils')

let scheduleDashboard = function (_project, _data) {
  let keyMilestone,
    threeMonthLookahead,
    keyMilestoneCommentary,
    objConfigKeyMilestone,
    objConfig3MthLookahead,
    objConfigCommentary

  renderSection()

  function renderSection () {
    objConfigCommentary = {
      dataSource: utils.filterByProject(_project, _data['Milestones - Commentary']),
      workstream: _project,
      containerDivId: 'milestones-commentary-1',
      textAlign: 'justify'
    }

    objConfigKeyMilestone = {
      dataSource: utils.filterByProject(_project, _data['Key Milestones Data']),
      containerDivId: 'gantt-chart-1',
      workstream: _project
    }

    objConfig3MthLookahead = {
      dataSource: utils.filterByProject(_project, _data['3-Month Lookahead Data']),
      containerDivId: 'gantt-chart-2',
      workstream: _project
    }

    keyMilestone = KeyMilestonesDashboardFactory(objConfigKeyMilestone)
    threeMonthLookahead = ThreeMonthLookaheadDashboardFactory(objConfig3MthLookahead)
    keyMilestoneCommentary = CommentaryFactory(objConfigCommentary)

    keyMilestone.drawGantt()
    threeMonthLookahead.drawGantt()
    keyMilestoneCommentary.drawCommentary()
  }
}

module.exports = scheduleDashboard
