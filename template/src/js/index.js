'use strict'
import './imports'

const startApp = require('./dash-start-app')
const config = require('./dash-config')
const loadMenuDashboard = require('./dash-load-menu')
const programmeOnPageDashboard = require('./dash-programme-on-page')
const headerDashboard = require('./dash-header')
const progressDashboard = require('./dash-progress')
const issueDashboard = require('./dash-issue')
const changeControlDashboard = require('./dash-change-control')
const documentControlDashboard = require('./dash-document-control')
const safetyDashboard = require('./dash-safety')
const sustainabilityDashboard = require('./dash-sustainability')
const riskOppDashboard = require('./dash-risk')
const topIndicatorsDashboard = require('./dash-top-indicators')
const commercialDashboard = require('./dash-commercial')
const scheduleDashboard = require('./dash-schedule')

// Global Variables
window._data = {}
window._project = 'Select Project'
window._user = {};

(function () {
  let utils = require('./dash-utils')
  let $workStreamSelected

  utils.applyLoadingOverlay()
  utils.setCopyrightYear('copyright-year')
  utils.setSelectedProject(window._project)

  startApp(config, loadHandlers)

  function loadHandlers () {
    utils.removeLoadingOverlay()
    loadMenuDashboard(window._data)
    programmeOnPageDashboard(window._project, window._data)
    headerDashboard(window._project, window._data)
    $('#wrapper').css('opacity', '1')
    utils.showInitialFragment('#programme-on-page-dashboard-div')

    utils.setSelectedProject(window._project)

    $('.section-btn:not(#schedule-btn)').off('click').on('click', function (e) {
      let sectionDiv = $(this).data('section-div')
      $('#' + sectionDiv + ', .page-title-box').show()
      $('.dashboard-div:not(#' + sectionDiv + ')').hide()

      drawDashboard()

      if (sectionDiv === 'programme-on-page-dashboard-div') {
        programmeOnPageDashboard(window._project, window._data)
        $('.section-btn:not(#programme-on-page-btn)').addClass('disable')
        utils.setSelectedProject('Select Project')
        $('.page-title-box').addClass('hidden')
        $('.counters-container').addClass('hidden')
      }
    })

    // Update top dropdown with the corresponding workstream
    // and call drawDashboard() to update data and charts
    $workStreamSelected = $('.workstream-menu')
    $workStreamSelected.on('click', function () {
      window._project = $(this).data('workstream')
      $('.counters-container').removeClass('hidden')
      $('.section-btn').removeClass('disable')
      $('.workstream-dropdown-title').html(window._project + ' <span class="caret"></span>')

      $('.page-title-box').removeClass('hidden')

      if ($('#programme-on-page-dashboard-div').css('display') === 'block') {
        $('#programme-on-page-dashboard-div').hide()
        $('#programme-on-page-btn > a').removeClass('subdrop')
        $('#progress-btn').click().find('a').addClass('subdrop')
      }

      drawDashboard()
    })
  }

  function drawDashboard () {
    // The dashboard is composed of a number of "modules" that expose
    // a function to build the relevant content. Populate the array
    // with the set of required modules
    const sectionBuilders = [
      programmeOnPageDashboard,
      progressDashboard,
      issueDashboard,
      changeControlDashboard,
      documentControlDashboard,
      safetyDashboard,
      sustainabilityDashboard,
      riskOppDashboard,
      headerDashboard,
      topIndicatorsDashboard,
      commercialDashboard,
      scheduleDashboard
    ]

    // Each of the modules listed above will be processed by
    // the following function
    const processSection = function (section) {
      try {
        section(window._project, window._data)
      } catch (ex) {
        console.error('Errors Raised in main drawDashboard section...')
        console.error(ex)
      }
    }

    // Process the array of section modules
    R.forEach(processSection)(sectionBuilders)
  }
})()
