'use strict'
const utils = require('./dash-utils')

let headerDashboard = function (_project, _data) {
  let $projectTitle,
    $projectManagerText,
    $projectSponsorText,
    $mplSponsorText,
    $budgetHolderText,
    $costManagerText,
    $mplBudgetHolderText,
    filteredData,
    projectTitleData,
    projectManagerData,
    projectSponsorData,
    mplSponsorData,
    budgetHolderData,
    costManagerData,
    mplBudgetHolderData,
    insertHeader

  renderSection()

  function renderSection () {
    filteredData = _project === 'Select Project'
      ? _data['Header Info Data']
      : _data['Header Info Data']
          .filter(obj => obj.Project === _project)
          .reduce(curr => curr)

    $projectTitle = $('#project-title')
    $projectManagerText = $('#project-manager-text')
    $projectSponsorText = $('#project-sponsor-text')
    $mplSponsorText = $('#mpl-sponsor-text')
    $budgetHolderText = $('#budget-holder-text')
    $costManagerText = $('#cost-manager-text')
    $mplBudgetHolderText = $('#mpl-budget-holder-text')
    projectTitleData = filteredData['Project']
    projectManagerData = filteredData['Project Manager']
    projectSponsorData = filteredData['Project Sponsor']
    mplSponsorData = filteredData['Design Integrator']
    budgetHolderData = filteredData['Budget Holder']
    costManagerData = filteredData['Cost Manager']
    mplBudgetHolderData = filteredData['MP&L Budget Holder']

    insertHeader = function () {
      if (_project !== 'Select Project') {
        $('.header-list-block').removeClass('hidden')
        $projectTitle.html(projectTitleData)
        $projectManagerText.html(projectManagerData)
        $projectSponsorText.html(projectSponsorData)
        $mplSponsorText.html(mplSponsorData)
        $budgetHolderText.html(budgetHolderData)
        $costManagerText.html(costManagerData)
        $mplBudgetHolderText.html(mplBudgetHolderData)
      }
    }

    insertHeader()
    utils.removeLoadingOverlay()
  }
}

module.exports = headerDashboard
