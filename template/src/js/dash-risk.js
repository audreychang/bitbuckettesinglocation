'use strict'
const GoogleChartFactory = require('./fact-google-chart')
const DataTableFactory = require('./fact-data-table')
const CommentaryFactory = require('./fact-commentary')
const utils = require('./dash-utils')

let riskOppDashboard = function (_project, _data) {
  let objConfigRiskOppExposure = {}
  let riskOppExposure,
    riskTop5,
    oppTop5,
    objConfigTop5Risks,
    objConfigTop5Opps,
    pattern,
    sourceColumns,
    destinationColumn,
    objConfigRagCommentary,
    ragCommentary

  renderSection()

  function renderSection () {
        // const h = utils.getHeaders(_data['Risk & Opportunities - Exposure']);
    objConfigRiskOppExposure = {
      barColumnChart: 'ComboChart',
      dataSource: utils.filterByProject(_project, _data['Risks & Opportunities - Exposure']),
      workstream: _project,
      containerDivId: 'risk-bar-chart-1',
      workstreamColumn: 4,
      viewColumns: [
        'Period Start Date',
        'Forecast Opp Balance',
        'Forecast Risk Balance',
        'Contingency'
      ],
      legendPosition: 'top',
      chartAreaSettings: {
        top: 50,
        bottom: 50
      },
      seriesSettings: {
        0: {
          color: '#42BDEF'
        },
        1: {
          color: '#FF5454'
        },
        2: {
          type: 'bars',
          color: '#A5A5A5'
        }
      },
      convertColumns: [{
        colIndex: 1,
        dataType: 'number'
      },
      {
        colIndex: 2,
        dataType: 'number'
      },
      {
        colIndex: 3,
        dataType: 'number'
      }
      ],
      hAxisSettings: {
        textStyle: {
          fontSize: 12
        }
      },
      vAxisSettings: {
        textStyle: {
          fontSize: 14
        }
      },
      predicateFunction: function (arr) {
        return arr[1] === '0' && arr[2] === '0' && (arr[3] === '0' || arr[3] === '')
      }
    }

    objConfigTop5Risks = {
      dataSource: utils.filterByProject(_project, _data['Risks & Opportunities - Top 5 Risks']),
      containerDivId: 'risk-data-table-1',
      workstream: _project,
      workstreamColumn: 6,
      viewColumns: [0, 1, 2],
      headerRowClass: 'soft-table-header',
      headerCellClass: 'dark-border',
      tableCellClass: 'table-cell',
      counterContainerId: 'issue-counter',
      hoverTableRowClass: 'dark-hover',
      oddTableRowClass: 'light-background'
    }

    objConfigTop5Opps = {
      dataSource: utils.filterByProject(_project, _data['Risks & Opportunities - Top 5 Opportunities']),
      containerDivId: 'risk-data-table-2',
      workstream: _project,
      workstreamColumn: 6,
      viewColumns: [0, 1, 2],
      headerRowClass: 'soft-table-header',
      headerCellClass: 'dark-border',
      tableCellClass: 'table-cell',
      counterContainerId: 'issue-counter',
      hoverTableRowClass: 'dark-hover',
      oddTableRowClass: 'light-background'
    }

    objConfigRagCommentary = {
      dataSource: utils.filterByProject(_project, _data['Risks & Opportunities - RAG Commentary']),
      workstream: _project,
      containerDivId: 'risk-rag-commentary',
      textAlign: 'justify'
    }

    pattern = '<div class="rsk-score" style="background-color: {1}; color: {2}; width: 100%; text-align: right; padding: 10px 10px 10px 10px; border-radius: 1px;">{0}</div>'
    sourceColumns = [2, 4, 5]
    destinationColumn = 2

    riskOppExposure = GoogleChartFactory(objConfigRiskOppExposure)
    riskOppExposure.drawBarChart()

    riskTop5 = DataTableFactory(objConfigTop5Risks)
    riskTop5.applyPatternFormat(pattern, sourceColumns, destinationColumn).drawDataTable()

    oppTop5 = DataTableFactory(objConfigTop5Opps)
    oppTop5.applyPatternFormat(pattern, sourceColumns, destinationColumn).drawDataTable()

    ragCommentary = CommentaryFactory(objConfigRagCommentary)
    ragCommentary.drawCommentary()
  }
}

module.exports = riskOppDashboard
